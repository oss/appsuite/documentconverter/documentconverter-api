/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.json;

import java.io.InputStream;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.ajax.fileholder.IFileHolder;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.FileAndURL;
import com.openexchange.documentconverter.FileIdManager;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.client.impl.ClientConfig;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageCapability;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.composition.FolderID;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.groupware.attach.Attachments;
import com.openexchange.java.Streams;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.compose.Attachment;
import com.openexchange.mail.compose.AttachmentResult;
import com.openexchange.mail.compose.CompositionSpaceId;
import com.openexchange.mail.compose.CompositionSpaceService;
import com.openexchange.mail.compose.CompositionSpaceServiceFactoryRegistry;
import com.openexchange.mail.compose.CompositionSpaces;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

/**
 * {@link DocumentConverterAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public abstract class DocumentConverterAction implements AJAXActionService {

    // - Statics ---------------------------------------------------------------

    // TODO (KA): check list of parameters for completeness prior to every release!!!
    // The parameter list is used for the fast detection of ManagedFiles, based on
    // the set of all given attributes to create a unique identifier. The ManagedFile itself
    // is created for the speedup of chunked requests, using the same source again and again
    final private static String[] ID_PARAMS = { "uid", "id", "folder_id", "version", "revtag", "source", "attached", "module", "cryptoAuth", "attachmentId" };

    final protected static String FORMAT_JSON = "json";

    final protected static String FORMAT_FILE = "file";

    final protected static String JSON_KEY_ORIGIN = "origin";

    final protected static String JSON_KEY_ERRORCODE = "errorCode";

    final protected static String JSON_KEY_ERRORDATA = "errorData";

    final protected static String JSON_KEY_ERRORTEXT = "cause";

    final protected static String JSON_KEY_INVALID_CHARS = "invalidCharacters";

    final protected static String JSON_VALUE_DOUMENTCONVERTER = "DocumentConverter";

    final protected static String FILE_SCHEMA = "file:///";

    final protected static String MAIL_FOLDER = "Mail/";

    /**
     * Initializes a new {@link DocumentConverterAction}.
     *
     * @param services
     */
    public DocumentConverterAction(final ServiceLookup services, final ClientManager manager, final FileIdManager fileIdManager, final ClientConfig clientConfig) {
        super();

        m_services = services;
        m_manager = manager;
        m_fileIdManager = fileIdManager;
        m_clientConfig = clientConfig;
    }

    /**
     * @param request
     * @param session
     * @param jobError
     * @return
     */
    public FileAndURL getRequestSourceFileAndURL(
            @NonNull final AJAXRequestData request,
            @NonNull final ServerSession session,
            @NonNull final MutableWrapper<Boolean> documentDecrypted,
            @NonNull final MutableWrapper<JobErrorEx> jobErrorExWrapper) {

        final String documentId = getDocumentId(request, session);
        FileAndURL ret = null;

        if (null != documentId) {
            try {
                m_fileIdManager.lock(documentId);

                if (null == (ret = m_fileIdManager.getDataFileAndURL(documentId))) {
                    final String fileId = request.getParameter("id");
                    final String attachmentId = request.getParameter("attachmentId");
                    final String source = request.getParameter("source");
                    final String attached = request.getParameter("attached");
                    final String module = request.getParameter("module");
                    final String folderId = request.getParameter("folder_id");
                    final String version = request.getParameter("version");
                    final String cryptoAuth = request.getParameter("cryptoAuth");
                    final boolean mailCheck = ((attached != null) && (folderId != null) && (attached.length() > 0));
                    final boolean taskCheck = mailCheck && (module != null);
                    final boolean composeCheck = (fileId != null) && (attachmentId != null);
                    MailServletInterface mailServletInterface = null; // closing done in finally block
                    InputStream documentStm = null; // closing done in finally block
                    String documentURL = null;

                    try {
                        if (mailCheck && "mail".equals(source)) {
                            // mail attachment

                            if (StringUtils.isNotEmpty(cryptoAuth)) {
                                // If GuardMail, pull authentication from request
                                final CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory = m_services.getOptionalService(CryptographicServiceAuthenticationFactory.class);
                                String authentication = null;

                                if (null != encryptionAuthenticationFactory) {
                                    authentication = encryptionAuthenticationFactory.createAuthenticationFrom(request.optHttpServletRequest());
                                }

                                mailServletInterface = MailServletInterface.getInstanceWithDecryptionSupport(session, authentication);
                                documentDecrypted.set(Boolean.TRUE);
                            } else {
                                mailServletInterface = MailServletInterface.getInstance(session);
                            }

                            if (null != mailServletInterface) {
                                final MailPart mailPart = mailServletInterface.getMessageAttachment(folderId, fileId, attached, false);

                                if (null != mailPart) {
                                    final String fileName = mailPart.getFileName();

                                    documentStm = mailPart.getInputStream();
                                    documentURL = new StringBuilder(256).
                                            append(FILE_SCHEMA).
                                            append(MAIL_FOLDER).
                                            append(StringUtils.isNotBlank(fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN).toString();
                                }
                            }
                        } else if (composeCheck && "compose".equals(source)) {
                            // Composition space attachment
                            CompositionSpaceServiceFactoryRegistry registry = m_services.getOptionalService(CompositionSpaceServiceFactoryRegistry.class);
                            if (registry != null) {
                                CompositionSpaceId compositionSpaceId = new CompositionSpaceId(fileId);
                                UUID attachmentUuid = CompositionSpaces.parseAttachmentId(attachmentId);
                                CompositionSpaceService compositionSpaceService = registry.getFactoryFor(compositionSpaceId.getServiceId(), session).createServiceFor(session);
                                AttachmentResult attachmentResult = compositionSpaceService.getAttachment(compositionSpaceId.getId(), attachmentUuid);
                                Attachment attachment = attachmentResult.getFirstAttachment();

                                if (null != attachment) {
                                    documentStm = attachment.getData();
                                } else {
                                    DocumentConverterManager.logError("DC client could not get attachment from CompositionSpaceService");
                                }
                            }
                        } else if (taskCheck && ("tasks".equals(source) || "calendar".equals(source) || "contacts".equals(source))) {
                            // other attachment from tasks, calendar or contact

                            documentStm = Attachments.getInstance().getAttachedFile(
                                    session,
                                    Integer.parseInt(folderId),
                                    Integer.parseInt(attached),
                                    Integer.parseInt(module),
                                    Integer.parseInt(fileId),
                                    session.getContext(),
                                    session.getUser(),
                                    session.getUserConfiguration());

                            documentURL = getFolderIdAndFileIdBasedFileUrl(session, folderId, fileId, null);
                        } else {
                            // use FileStore as default data source
                            final IDBasedFileAccessFactory fileFactory = m_services.getService(IDBasedFileAccessFactory.class);
                            IDBasedFileAccess fileAccess = (null != fileFactory) ? fileFactory.createAccess(session) : null;

                            if ((null != fileAccess) && "guard".equals(source)) {
                                if (StringUtils.isNotEmpty(cryptoAuth)) {
                                    // If GuardMail, get authentication
                                    final CryptographicAwareIDBasedFileAccessFactory encryptionAwareFileAccessFactory = m_services.getOptionalService(CryptographicAwareIDBasedFileAccessFactory.class);
                                    final CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory = m_services.getOptionalService(CryptographicServiceAuthenticationFactory.class);

                                    if ((null != encryptionAwareFileAccessFactory) && (null != encryptionAuthenticationFactory)) {
                                        final EnumSet<CryptographyMode> cryptMode = EnumSet.of(CryptographyMode.DECRYPT);

                                        fileAccess = encryptionAwareFileAccessFactory.createAccess(fileAccess, cryptMode, session,
                                                encryptionAuthenticationFactory.createAuthenticationFrom(request.optHttpServletRequest()));

                                        documentDecrypted.set(Boolean.TRUE);
                                    } else {
                                        // file is encrypted, but we can't decrypt...
                                        fileAccess = null;
                                    }
                                } else {
                                    fileAccess = null;
                                }

                                // set output paramater jobError to JobError.PASSWORD in
                                // case we got no valid fileAccess for given Guard parameters
                                if (null == fileAccess) {
                                    jobErrorExWrapper.set(new JobErrorEx(JobError.PASSWORD));
                                }
                            }

                            if (null != fileAccess) {
                                // #46555 map version String("null") to null
                                String versionToUse = "null".equalsIgnoreCase(version) ? FileStorageFileAccess.CURRENT_VERSION : version;

                                // #46555 check for versioning support
                                if (FileStorageFileAccess.CURRENT_VERSION != versionToUse) {
                                    final FolderID folderIdentifier = new FolderID(folderId);

                                    if (!fileAccess.supports(folderIdentifier.getService(), folderIdentifier.getAccountId(), FileStorageCapability.FILE_VERSIONS)) {
                                        versionToUse = FileStorageFileAccess.CURRENT_VERSION;
                                    }
                                }

                                documentStm = fileAccess.getDocument(fileId, versionToUse);
                                documentURL = getFolderIdAndFileIdBasedFileUrl(session, folderId, fileId, versionToUse);
                            }
                        }

                        if (null != documentStm) {
                            ret = m_fileIdManager.setDataInputStreamAndURL(documentId, documentStm, documentURL);
                        } else {
                            final JobErrorEx jobErrorEx = jobErrorExWrapper.get();
                            final JobError jobError = jobErrorEx.getJobError();

                            if ((JobError.NONE != jobError) && (JobError.PASSWORD != jobError)) {
                                jobErrorExWrapper.set(new JobErrorEx(JobError.SOURCE_NOT_ACCESSIBLE));
                            }
                        }
                    } catch (final Exception e) {
                        jobErrorExWrapper.set(new JobErrorEx(JobError.SOURCE_NOT_ACCESSIBLE));
                        DocumentConverterManager.logExcp(e);
                    } finally {
                        Streams.close(documentStm);

                        if (mailServletInterface != null) {
                            mailServletInterface.close();
                        }
                    }
                }
            } finally {
                m_fileIdManager.unlock(documentId);
            }
        }

        // set output parameter for decrypted document to
        // FALSE if we have no document to return at all
        if (null == ret) {
            documentDecrypted.set(Boolean.FALSE);
        }

        return ret;
    }

    /**
     * @param request
     * @param session
     * @param isAsync
     * @param resultJobError
     * @return
     */
    protected @Nullable FileAndURL getPDFFileAndURL(
            @NonNull final AJAXRequestData request,
            @NonNull final ServerSession session,
            final boolean isAsync,
            @NonNull final MutableWrapper<Boolean> documentDecrypted,
            @NonNull final MutableWrapper<String> resultJobId,
            @NonNull final MutableWrapper<JobErrorEx> resultJobErrorExWrapper) {

        final IDocumentConverter docConverter = m_services.getService(IDocumentConverter.class);
        FileAndURL resultFileAndURL = null;

        resultJobErrorExWrapper.set(new JobErrorEx(JobError.GENERAL));

        if (null != docConverter) {
            final String locale = getPreferredLanguage(session);

            if (null == resultJobId.get()) {
                final String documentId = getDocumentId(request, session, "pdf", locale);

                if (null == documentId) {
                    return null;
                }

                resultJobId.set(documentId);
            }

            try {
                m_fileIdManager.lock(resultJobId.get());

                if (null == (resultFileAndURL = m_fileIdManager.getDataFileAndURL(resultJobId.get()))) {
                    final FileAndURL sourceFileAndURL = getRequestSourceFileAndURL(request, session, documentDecrypted, resultJobErrorExWrapper);

                    if((null != sourceFileAndURL) && ((null != sourceFileAndURL.getFile()))) {
                        final String fileName = request.getParameter("filename");

                        if ((null != fileName) && "pdf".equalsIgnoreCase(FilenameUtils.getExtension(fileName))) {
                            // return source file in case of a PDF source
                            try (final InputStream srcInputStm = FileUtils.openInputStream(sourceFileAndURL.getFile())) {
                                resultFileAndURL = m_fileIdManager.setDataInputStreamAndURL(resultJobId.get(), srcInputStm, sourceFileAndURL.getURL());
                                resultJobErrorExWrapper.set(new JobErrorEx());
                            }
                        } else // convert to PDF via DocumentConverter
                            if (isAsync && documentDecrypted.get().booleanValue()) {
                                // we don't trigger any async conversion in case
                                // of a decrypted document and leave ASAP since
                                // we don't want to cache decrypted document
                                // conversion results for security reasons
                                resultJobErrorExWrapper.set(new JobErrorEx());
                            } else {
                                final HashMap<String, Object> jobProperties = new HashMap<>(12);
                                final HashMap<String, Object> resultProperties = new HashMap<>(8);

                                jobProperties.put(Properties.PROP_INPUT_FILE, sourceFileAndURL.getFile());
                                jobProperties.put(Properties.PROP_PRIORITY, JobPriority.fromString(request.getParameter("priority")));
                                jobProperties.put(Properties.PROP_LOCALE, locale);

                                final String inputURL = sourceFileAndURL.getURL();

                                if (null != inputURL) {
                                    jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                                }

                                if (null != fileName) {
                                    jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
                                }

                                if (isAsync) {
                                    jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
                                    jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
                                } else {
                                    // this is a user request in synchronous case
                                    jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
                                }

                                if (documentDecrypted.get().booleanValue()) {
                                    // set 'NO_CACHE=true' property for decrypted documents since we don't
                                    // want to cache decrypted document conversion results for security reasons
                                    jobProperties.put(Properties.PROP_NO_CACHE, Boolean.TRUE);
                                }

                                try (InputStream pdfResultStm = docConverter.convert("pdf", jobProperties, resultProperties, session)) {
                                    if (null != pdfResultStm) {
                                        resultFileAndURL = m_fileIdManager.setDataInputStreamAndURL(resultJobId.get(), pdfResultStm, sourceFileAndURL.getURL());
                                        resultJobErrorExWrapper.set(new JobErrorEx());
                                    } else {
                                        JobErrorEx pdfJobErrorEx = JobErrorEx.fromResultProperties(resultProperties);

                                        if (pdfJobErrorEx.hasNoError()) {
                                            pdfJobErrorEx = new JobErrorEx(JobError.GENERAL);
                                        }

                                        resultJobErrorExWrapper.set(isAsync ? new JobErrorEx() : pdfJobErrorEx);
                                    }
                                }
                            }
                    } else {
                        // async case for PDF documents
                        resultJobErrorExWrapper.set(new JobErrorEx(isAsync ? JobError.NONE : JobError.SOURCE_NOT_ACCESSIBLE));
                    }
                } else {
                    resultJobErrorExWrapper.set(new JobErrorEx());
                }
            } catch (Exception e) {
                DocumentConverterManager.logExcp(e);
            } finally {
                m_fileIdManager.unlock(resultJobId.get());
            }
        }

        return resultFileAndURL;
    }

    /**
     * @param request
     * @param fileHolder
     * @return
     */
    AJAXRequestResult getRequestResult(
            @NonNull AJAXRequestData request,
            @Nullable IFileHolder fileHolder,
            @Nullable JSONObject jsonObject,
            @NonNull JobErrorEx jobErrorEx) {

        AJAXRequestResult ret = null;

        if (jobErrorEx.hasNoError() && (null != fileHolder)) {
            request.setFormat(FORMAT_FILE);
            ret = new AJAXRequestResult(fileHolder, FORMAT_FILE);
        } else {
            final JSONObject jsonResult = (null != jsonObject) ? jsonObject : new JSONObject();

            // TODO (KA): JobErrorEx!!!
            try {
                jsonResult.put(JSON_KEY_ORIGIN, JSON_VALUE_DOUMENTCONVERTER);

                if (jobErrorEx.hasError()) {
                    if (!jsonResult.has(JSON_KEY_ERRORTEXT)) {
                        jsonResult.put(JSON_KEY_ERRORTEXT,  jobErrorEx.getErrorText());
                    }

                    if (!jsonResult.has(JSON_KEY_ERRORDATA)) {
                        jsonResult.put(JSON_KEY_ERRORDATA,  jobErrorEx.getErrorData());
                    }
                }
            } catch (JSONException e) {
                DocumentConverterManager.logExcp(e);
            }

            request.setFormat(FORMAT_JSON);
            ret = new AJAXRequestResult(jsonResult, FORMAT_JSON);
        }

        return ret;
    }

    /**
     * @param session
     * @return
     */
    protected static @NonNull String getPreferredLanguage(ServerSession session) {
        final User sessionUser = session.getUser();
        String prefLanguage = null;

        // user language
        if (null != sessionUser) {
            prefLanguage = sessionUser.getPreferredLanguage();
        }

        return StringUtils.isBlank(prefLanguage) ? "en_US" : prefLanguage;
    }

    /**
     * @param request
     * @return
     */
    protected static @Nullable String getDocumentId(
            @NonNull AJAXRequestData request,
            ServerSession session,
            String... additionalData) {

        final StringBuilder documentIdBuilder = new StringBuilder(256);
        final String sessionId = request.getParameter("session");

        if ((null != session) && StringUtils.isNotEmpty(sessionId) && sessionId.equals(session.getSessionID())) {
            documentIdBuilder.append(sessionId).append('-');

            for (final String[] curParamArray : new String[][] { ID_PARAMS, additionalData }) {
                if (null != curParamArray) {
                    final boolean isParam = curParamArray.equals(ID_PARAMS);

                    for (final String curValue : curParamArray) {
                        documentIdBuilder.append(isParam ? request.getParameter(curValue) : curValue).append('~');
                    }
                }
            }
        }

        return (documentIdBuilder.length() > 0) ?
                DigestUtils.sha256Hex(documentIdBuilder.toString()) :
                    null;
    }

    /**
     * @param session
     * @param folderId
     * @param fileId
     * @param versionToUse providing <code>null</code> here uses the CURRENT_VERSION
     * @return
     */
    protected String getFolderIdAndFileIdBasedFileUrl(@NonNull final ServerSession session, final String folderId, final String fileId, @Nullable final String versionToUse) {
        final StringBuilder fileURLBuilder = new StringBuilder(512);

        if ((null != folderId) && (null != fileId)) {
            try {
                final IDBasedFileAccessFactory fileFactory = m_services.getService(IDBasedFileAccessFactory.class);
                IDBasedFileAccess fileAccess = (null != fileFactory) ? fileFactory.createAccess(session) : null;
                final File fileMetadata = (null != fileAccess) ? fileAccess.getFileMetadata(fileId, versionToUse) : null;

                if (null != fileMetadata) {
                    final IDBasedFolderAccessFactory folderFactory = m_services.getService(IDBasedFolderAccessFactory.class);

                    fileURLBuilder.append(FILE_SCHEMA);

                    // append all folders to URL
                    if (null != folderFactory) {
                        IDBasedFolderAccess folderAccess = folderFactory.createAccess(session);

                        if (null != folderAccess) {
                            FileStorageFolder[] fileStorageFolders = folderAccess.getPath2DefaultFolder(folderId);

                            if (null != fileStorageFolders) {
                                for (int i = fileStorageFolders.length - 1; i >= 0; --i) {
                                    fileURLBuilder.append(fileStorageFolders[i].getName()).append('/');
                                }
                            }
                        }
                    }

                    // append filename to URL
                    fileURLBuilder.append(fileMetadata.getFileName());
                }
            } catch (OXException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        return fileURLBuilder.toString();
    }

    // - Members ---------------------------------------------------------------

    final protected ServiceLookup m_services;

    final protected ClientManager m_manager;

    final protected ClientConfig m_clientConfig;

    final protected FileIdManager m_fileIdManager;
}
