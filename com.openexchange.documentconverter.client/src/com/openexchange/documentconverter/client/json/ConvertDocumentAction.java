/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.json;

import java.io.File;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import com.openexchange.ajax.container.ByteArrayInputStreamClosure;
import com.openexchange.ajax.container.FileHolder;
import com.openexchange.ajax.fileholder.IFileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.FileAndURL;
import com.openexchange.documentconverter.FileIdManager;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.PDFExtractor;
import com.openexchange.documentconverter.client.impl.ClientConfig;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link ConvertDocumentAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
class ConvertDocumentAction extends DocumentConverterAction {

    /**
     * Initializes a new {@link ConvertDocumentAction}.
     *
     * @param services
     */
    public ConvertDocumentAction(final ServiceLookup services, final ClientManager manager, final FileIdManager fileIdManager, final ClientConfig clientConfig) {
        super(services, manager, fileIdManager, clientConfig);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData,
     * com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) {
        final PDFExtractor pdfExtractor = m_manager.getPDFExtractor();
        final IDocumentConverter manager = m_services.getService(IDocumentConverter.class);

        if ((null != pdfExtractor) && (null != request)  && (null != session) && (null != manager)) {
            String convertAction = StringUtils.lowerCase(request.getParameter("convert_action"));

            if (null != convertAction) {
                IFileHolder resultFileHolder = null;
                JSONObject jsonResult = new JSONObject();
                final MutableWrapper<Boolean> documentDecrypted = new MutableWrapper<>(Boolean.FALSE);
                final MutableWrapper<JobErrorEx> jobErrorEx = new MutableWrapper<>(new JobErrorEx(JobError.GENERAL));

                if ("beginconvert".equals(convertAction)) {
                    performBeginConvert(
                        request,
                        session,
                        documentDecrypted,
                        jsonResult,
                        jobErrorEx);
                } else if ("getpage".equals(convertAction)) {
                    resultFileHolder = performGetPage(request, jobErrorEx);
                } else if ("endconvert".equals(convertAction)) {
                    performEndConvert(request, jobErrorEx);
                }

                return getRequestResult(request, resultFileHolder, jsonResult, jobErrorEx.get());
            }
        }

        return new AJAXRequestResult(null, FORMAT_JSON);
    }

    /**
     * @param request
     * @param session
     * @param manager
     * @param convertPriority
     * @param jsonResult
     * @param jobError
     */
    private void performBeginConvert(@NonNull AJAXRequestData request, @
        NonNull ServerSession session,
        @NonNull MutableWrapper<Boolean> documentDecrypted,
        @NonNull JSONObject jsonResult,
        @NonNull MutableWrapper<JobErrorEx> jobErrorExWrapper) {

        final MutableWrapper<String> jobId = new MutableWrapper<>(UUID.randomUUID().toString());
        final FileAndURL pdfResultFileAndURL = getPDFFileAndURL(request, session, false, documentDecrypted, jobId, jobErrorExWrapper);

        if ((null != pdfResultFileAndURL) && (null != pdfResultFileAndURL.getFile())) {
            try {
                // resource is held with a configured timeout of AUTO_CLEANUP_TIMEOUT_MILLIS
                m_fileIdManager.lock(jobId.get(), m_clientConfig.AUTO_CLEANUP_TIMEOUT_MILLIS);

                final PDFExtractor pdfExtractor = m_manager.getPDFExtractor();

                if (null != pdfExtractor) {
                    final String fileName = request.getParameter("filename");
                    final int pageCount = pdfExtractor.getPageCount(pdfResultFileAndURL.getFile(), fileName);

                    if (pageCount > 0) {
                        jsonResult.put("jobID", jobId.get());
                        jsonResult.put("pageCount", pageCount);

                        jobErrorExWrapper.set(new JobErrorEx());
                    }
                } else {
                    DocumentConverterManager.logError("DC client Ajax #beginConvert handler has no valid PDFExtractor to begin conversion of PDF file: " + jobId);
                    jobErrorExWrapper.set(new JobErrorEx(JobError.PDFTOOL));
                }
            } catch (Exception e) {
                jobErrorExWrapper.set(new JobErrorEx(JobError.PDFTOOL));
                DocumentConverterManager.logExcp(e);
            } finally {
                m_fileIdManager.unlock(jobId.get());
            }
        }
    }

    /**
     * @param request
     * @param session
     * @param manager
     * @param jsonResult
     * @param jobError
     * @return
     */
    private IFileHolder performGetPage(@NonNull AJAXRequestData request, @NonNull MutableWrapper<JobErrorEx> jobErrorExWrapper) {
        IFileHolder resultFileHolder = null;
        final String jobId = request.getParameter("job_id");
        final String targetFormat = request.getParameter("target_format");
        final String pageNumberStr = request.getParameter("page_number");
        final int pageNumber = (null != pageNumberStr) ? Integer.parseInt(pageNumberStr) : 0;
        final boolean isPngTarget = "png".equalsIgnoreCase(targetFormat);
        final boolean isJpegTarget = !isPngTarget && ("jpg".equalsIgnoreCase(targetFormat) || "jpeg".equalsIgnoreCase(targetFormat));
        final boolean isSVGTarget = !isPngTarget && !isJpegTarget && "svg".equalsIgnoreCase(targetFormat);

        jobErrorExWrapper.set(new JobErrorEx(JobError.GENERAL));

        if (StringUtils.isNotBlank(jobId) && (pageNumber > 0) && (isPngTarget || isJpegTarget|| isSVGTarget)) {
            File tmpOutputFile = null;

            try {
                m_fileIdManager.lock(jobId);

                final PDFExtractor pdfExtractor = m_manager.getPDFExtractor();
                final FileAndURL pdfInputFileAndURL = m_fileIdManager.getDataFileAndURL(jobId);

                if ((null == pdfInputFileAndURL) || (null == pdfInputFileAndURL.getFile())) {
                    DocumentConverterManager.logError("DC client Ajax #getPage handler got no input file for PDF document. The input file has been removed by auto cleanup handler in the meantime: " + jobId);
                    jobErrorExWrapper.set(new JobErrorEx(JobError.SOURCE_NOT_ACCESSIBLE));
                } else if (null == pdfExtractor) {
                    DocumentConverterManager.logError("DC client Ajax #getPage handler has no valid PDFExtractor to get page from PDF file : " + jobId);
                    jobErrorExWrapper.set(new JobErrorEx(JobError.PDFTOOL));
                } else {
                    final String fileName = request.getParameter("filename");
                    final String targetWidthStr = request.getParameter("target_width");
                    final String targetHeightStr = request.getParameter("target_height");
                    final int pixelWidth = Integer.valueOf((null != targetWidthStr) ? targetWidthStr : "-1").intValue();
                    final int pixelHeight = Integer.valueOf((null != targetHeightStr) ? targetHeightStr : "-1").intValue();


                    tmpOutputFile = m_manager.createTempFile(jobId);

                    pdfExtractor.extractToGraphic(
                        pdfInputFileAndURL.getFile(),
                        tmpOutputFile,
                        targetFormat,
                        pageNumber,
                        pixelWidth,
                        pixelHeight,
                        "contain",
                        fileName);

                    // prepare Ajax result object
                    resultFileHolder = new FileHolder(
                        new ByteArrayInputStreamClosure(FileUtils.readFileToByteArray(tmpOutputFile)),
                        tmpOutputFile.length(),
                        isPngTarget ? "image/png" : (isSVGTarget ? "image/svg+xml" : "image/jpeg"),
                            new StringBuilder(32).append("pdfpage_").
                            append(pageNumber).
                            append(isPngTarget ? ".png" : (isSVGTarget ? ".svg" : ".jpg")).toString());

                    request.setFormat(FORMAT_FILE);
                    request.setProperty("target_width", Integer.valueOf(pixelWidth));
                    request.setProperty("target_height", Integer.valueOf(pixelHeight));

                    jobErrorExWrapper.set(new JobErrorEx());
                }
            } catch (Exception e) {
                jobErrorExWrapper.set(new JobErrorEx(JobError.PDFTOOL));
                DocumentConverterManager.logExcp(e);
            } finally {
                FileUtils.deleteQuietly(tmpOutputFile);
                m_fileIdManager.unlock(jobId);
            }
        }

        return resultFileHolder;
    }

    /**
     * @param request
     * @param session
     * @param manager
     * @param jsonResult
     * @param jobError
     */
    private void performEndConvert(@NonNull AJAXRequestData request, @NonNull MutableWrapper<JobErrorEx> jobErrorEx) {
        final String jobId = request.getParameter("job_id");

        if (StringUtils.isNotBlank(jobId)) {
            try {
                m_fileIdManager.lock(jobId);
                m_fileIdManager.delete(jobId);
            } finally {
                jobErrorEx.set(new JobErrorEx());
                m_fileIdManager.unlock(jobId);
            }
        } else {
            jobErrorEx.set(new JobErrorEx(JobError.SOURCE_NOT_ACCESSIBLE));
        }
    }
}
