/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.json;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXActionServiceFactory;
import com.openexchange.documentconverter.FileIdManager;
import com.openexchange.documentconverter.client.impl.ClientConfig;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.server.ServiceLookup;

/**
 * {@link DocumentConverterActionFactory}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class DocumentConverterActionFactory implements AJAXActionServiceFactory {

    private final Map<String, AJAXActionService> actions = new HashMap<>();

    /**
     * Initializes a new {@link DocumentConverterActionFactory}.
     * @param services
     */
    public DocumentConverterActionFactory(
        final ServiceLookup services,
        final ClientManager manager,
        final FileIdManager fileIdManager,
        final ClientConfig clientConfig) {

        super();

        actions.put("convertdocument", new ConvertDocumentAction(services, manager, fileIdManager, clientConfig));
        actions.put("getdocument", new GetDocumentAction(services, manager, fileIdManager, clientConfig));
    }

    /* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionServiceFactory#createActionService(java.lang.String)
     */
    @Override
    public AJAXActionService createActionService(final String action) {
        return (actions.get(action));
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentation.AnnotatedServices#getSupportedServices()
     */
    public Collection<? extends AJAXActionService> getSupportedServices() {
        return java.util.Collections.unmodifiableCollection(actions.values());
    }
}
