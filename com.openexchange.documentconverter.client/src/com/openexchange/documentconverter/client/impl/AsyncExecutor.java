/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.ICounter;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;

/**
 * {@link AsyncConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
/**
 * {@link AsyncExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
public class AsyncExecutor implements ICounter {

    final protected static int MAX_REQUEST_QEUE_SIZE = 16384;

    final protected static long CLEANUP_TIMEOUT_MILLIS = 600000;

    /**
     * {@link AsyncIdentifier}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.8.0
     */
    protected static class AsyncIdentifier {

        protected AsyncIdentifier(@NonNull final String asyncHash) {
            super();

            m_asyncHash = asyncHash;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return (31 + ((m_asyncHash == null) ? 0 : m_asyncHash.hashCode()));
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if ((obj == null) || getClass() != obj.getClass()) {
                return false;
            }

            AsyncIdentifier other = (AsyncIdentifier) obj;

            if (m_asyncHash == null) {
                if (other.m_asyncHash != null) {
                    return false;
                }
            } else if (!m_asyncHash.equals(other.m_asyncHash)) {
                return false;
            }

            return true;
        }

        // - API ---------------------------------------------------------------

        /**
         * @return
         */
        protected String getAsyncHash() {
            return m_asyncHash;
        }

        /**
         * @return
         */
        protected long getTimestampMillis() {
            return m_timestampMillis;
        }

        // - Members -----------------------------------------------------------

        final private String m_asyncHash;

        final private long m_timestampMillis = System.currentTimeMillis();
    }

    /**
     * {@link AsyncRunnable}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.8.0
     */
    protected class AsyncRunnable implements Runnable {

        /**
         * Initializes a new {@link AsyncRunnable}.
         * @param jobType
         * @param jobProperties
         */
        protected AsyncRunnable(@NonNull String jobType, @NonNull String cacheHash, @NonNull HashMap<String, Object> jobProperties) {
            m_jobType = jobType;
            m_asyncId = new AsyncIdentifier(cacheHash);
            m_jobProperties = new HashMap<>(jobProperties.size());
            m_jobProperties.putAll(jobProperties);

            final File inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

            try (final InputStream inputStm = (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM)) {
                if (StringUtils.isNotEmpty(m_jobType) && ((null != inputFile) || (null != inputStm))) {
                    final File tempInputFile = m_manager.createTempFile("oxasync");

                    if (null != tempInputFile) {
                        if (null != inputFile) {
                            FileUtils.copyFile(inputFile, tempInputFile);
                            m_jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                        } else if (null != inputStm) {
                            FileUtils.copyInputStreamToFile(inputStm, tempInputFile);

                            // replace the original input content file temp. input file
                            m_jobProperties.remove(Properties.PROP_INPUT_STREAM);
                            m_jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                        }
                    }
                }
            } catch (final Exception e) {
                DocumentConverterManager.logExcp(e);
            }

            final JobPriority jobPriority = (JobPriority) m_jobProperties.get(Properties.PROP_PRIORITY);
            if (null == jobPriority) {
                m_jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
            }
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            // reduce parent queue count by one
            decrement();

            if (isValid()) {
                logDebug("DC started asynchronous conversion", m_jobProperties);

                final HashMap<String, Object> resultProperties = new HashMap<>(8);
                DocumentConverterManager.close(ManagedClientConverterJob.process(m_jobType, m_jobProperties, resultProperties));

                // increment run counter by one
                m_ranCounter.incrementAndGet();

                logDebug("DC finished asynchronous conversion", m_jobProperties);
            }
            clear();
        }

        /**
         * @return
         */
        protected HashMap<String, Object> getJobProperties() {
            return m_jobProperties;
        }

        /**
         * @return
         */
        protected AsyncIdentifier getAsyncIdentifier() {
            return m_asyncId;
        }

        /**
         * @return
         */
        protected synchronized boolean isValid() {
            return !m_jobProperties.isEmpty();
        }

        /**
         *
         */
        protected synchronized void clear() {
            FileUtils.deleteQuietly((File) m_jobProperties.remove(Properties.PROP_INPUT_FILE));
            m_jobProperties.clear();
        }

        // - Members -------------------------------------------------------

        protected final String m_jobType;
        protected final AsyncIdentifier m_asyncId;
        protected final HashMap<String, Object> m_jobProperties;
    }

    /**
     * Initializes a new {@link AsyncExecutor}.
     * @param manager
     * @param maxThreadCount
     */
    public AsyncExecutor(@NonNull IDocumentConverter converter, @NonNull DocumentConverterManager manager, int maxThreadCount, int queueSize) {
        super();

        m_documentConverter = converter;
        m_manager = manager;

        m_requestQueue = new ArrayBlockingQueue<>(queueSize);

        if (maxThreadCount > 0) {
            m_requestExecutor = new ThreadPoolExecutor(maxThreadCount, maxThreadCount, 0, TimeUnit.MILLISECONDS, m_requestQueue);

            // ensure, that entry is cleaned up (e.g. remove tmp. files) if execution is rejected
            m_requestExecutor.setRejectedExecutionHandler((runnable, executor) -> {
                if (null != runnable) {
                    m_droppedCounter.incrementAndGet();
                    decrement();
                    ((AsyncRunnable) runnable).clear();
                }
            });

            // initialize timer to clean up run set
            m_timerExecutor.scheduleAtFixedRate(() -> {
                final long curTimeMillis = System.currentTimeMillis();

                synchronized (m_runSet) {
                    for (final Iterator<AsyncIdentifier> asyncIdIter = m_runSet.iterator(); asyncIdIter.hasNext();) {
                        if ((curTimeMillis - asyncIdIter.next().getTimestampMillis()) > CLEANUP_TIMEOUT_MILLIS) {
                            asyncIdIter.remove();
                        } else {
                            // since the items are ordered by time, we
                            // can stop the search when the first item
                            // did not reach its timeout
                            break;
                        }
                    }
                }
            }, CLEANUP_TIMEOUT_MILLIS, CLEANUP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        }
    }

    // - ICounter --------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#increment()
     */
    @Override
    public void increment() {
        // ok
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#decrement()
     */
    @Override
    public void decrement() {
        // ok
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#getCount()
     */
    @Override
    public int getScheduledCount() {
        return m_requestQueue.size();
    }

    // - public API ------------------------------------------------------------

    public void shutdown() {
        if (m_isRunning.compareAndSet(true, false)) {
            final boolean trace = DocumentConverterManager.isLogTrace();
            long traceStartTimeMillis = 0;

            AsyncExecutor.AsyncRunnable curRunnable = null;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                DocumentConverterManager.logTrace("DC AsyncExecutor starting shutdown...");
            }

            while((curRunnable = (AsyncExecutor.AsyncRunnable) m_requestQueue.poll()) != null) {
                // decrement queue count by one
                decrement();
                curRunnable.clear();
            }

            if (trace) {
                DocumentConverterManager.logTrace(new StringBuilder(256).
                    append("DC AsyncExecutor finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @return
     */
    boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     */
    public void triggerExecution(@NonNull String jobType, @NonNull String cacheHash, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        if (isRunning() && (null != m_requestExecutor) && (null != jobType) && (null != jobProperties) && (null != resultProperties)) {
            final AsyncExecutor.AsyncRunnable asyncRunnable = new AsyncExecutor.AsyncRunnable(jobType, cacheHash, jobProperties);

            if (needsRunning(asyncRunnable)) {
                logDebug("DC scheduled asynchronous job", asyncRunnable.getJobProperties());

                // increment queue count by one
                increment();
                m_requestExecutor.execute(asyncRunnable);

            } else {
                logDebug("DC already scheduled same asynchronous job => omitting request", asyncRunnable.getJobProperties());

                m_omitCounter.incrementAndGet();
                asyncRunnable.clear();
            }
        }
    }

    /**
     * @return
     */
    public long getProcessedCount() {
        return m_ranCounter.get();
    }

    /**
     * @return
     */
    public long getDroppedCount() {
        return m_droppedCounter.get();
    }

    /**
     * @return
     */
    public LogData[] getLogData() {
        return new LogData[] {
            new LogData("async_jobs_remaining", Long.toString(getScheduledCount())),
            new LogData("async_jobs_ran", m_ranCounter.toString()),
            new LogData("async_job_omitted", m_omitCounter.toString())
        };
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param asyncRunnable
     * @return
     */
    protected boolean needsRunning(@NonNull final AsyncRunnable asyncRunnable) {
        final AsyncIdentifier asyncId = asyncRunnable.getAsyncIdentifier();
        boolean ret = true;

        synchronized (m_runSet) {
            // if entry with same AsyncId is already contained
            // in run set => remove old entry and put new entry
            // with up to date timestamp at end of time based
            // ordered LinkedHashSet
            if (m_runSet.remove(asyncId)) {
                ret = false;
            }

            // put entry at end of the set in every case
            m_runSet.add(asyncId);
        }


        return ret;
    }

    /**
     * @param text
     * @param jobProperties
     */
    protected void logDebug(@NonNull final String text, @NonNull final HashMap<String, Object> jobProperties) {
        if (DocumentConverterManager.isLogDebug()) {
            DocumentConverterManager.logDebug(text, jobProperties, getLogData());
        }
    }

    // - Members -----------------------------------------------------------

    final protected IDocumentConverter m_documentConverter;
    final protected DocumentConverterManager m_manager;
    final protected BlockingQueue<Runnable> m_requestQueue;
    final protected LinkedHashSet<AsyncIdentifier> m_runSet = new LinkedHashSet<>();
    final protected ScheduledExecutorService m_timerExecutor = Executors.newScheduledThreadPool(1);
    final protected AtomicBoolean m_isRunning = new AtomicBoolean(true);
    final protected AtomicLong m_ranCounter = new AtomicLong(0);
    final protected AtomicLong m_omitCounter = new AtomicLong(0);
    final protected AtomicLong m_droppedCounter = new AtomicLong(0);
    protected ThreadPoolExecutor m_requestExecutor = null;
}
