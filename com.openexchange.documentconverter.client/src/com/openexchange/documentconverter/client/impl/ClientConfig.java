/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadables;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.NonNull;

/**
 * {@link Config}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link ClientConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class ClientConfig {

    /**
     * Config items
     */
    final private static String CONFIGITEM_REMOTE_DOCUMENTCONVERTER_URL = "com.openexchange.documentconverter.client.remoteDocumentConverterUrl";
    final private static String CONFIGITEM_CACHESERVICE_URL = "com.openexchange.documentconverter.client.cacheServiceUrl";
    final private static String CONFIGITEM_MAX_DOCUMENT_SOURCEFILE_SIZE_MB = "com.openexchange.documentconverter.client.maxDocumentSourceFileSizeMB";
    final private static String CONFIGITEM_ENABLE_CACHE_LOOKUP = "com.openexchange.documentconverter.enableCacheLookup";
    final private static String CONFIGITEM_SUPPORTED_TYPES = "com.openexchange.documentconverter.client.supportedTypes";
    final private static String CONFIGITEM_PDFEXTRACTOR_INSTALL_DIR = "com.openexchange.documentconverter.client.pdfextractor.installDir";
    final private static String CONFIGITEM_PDFEXTRACTOR_WORK_DIR = "com.openexchange.documentconverter.client.pdfextractor.workDir";
    final private static String CONFIGITEM_PDFEXTRACTOR_MAX_VMEM_MB = "com.openexchange.documentconverter.client.pdfextractor.maxVMemMB";
    final private static String CONFIGITEM_AUTO_CLEANUP_TIMEOUT_SECONDS = "com.openexchange.documentconverter.client.autoCleanupTimeoutSeconds";


    /**
     * All config items
     */
    final private static String[] ALL_CONFIGITEMS = {
        CONFIGITEM_REMOTE_DOCUMENTCONVERTER_URL,
        CONFIGITEM_CACHESERVICE_URL,
        CONFIGITEM_MAX_DOCUMENT_SOURCEFILE_SIZE_MB,
        CONFIGITEM_ENABLE_CACHE_LOOKUP,
        CONFIGITEM_SUPPORTED_TYPES,
        CONFIGITEM_PDFEXTRACTOR_INSTALL_DIR,
        CONFIGITEM_PDFEXTRACTOR_WORK_DIR,
        CONFIGITEM_PDFEXTRACTOR_MAX_VMEM_MB,
        CONFIGITEM_AUTO_CLEANUP_TIMEOUT_SECONDS
    };

    /**
     * Reloadable config items
     */
    final private static String[] RELOADABLE_CONFIGITEMS = {
        CONFIGITEM_REMOTE_DOCUMENTCONVERTER_URL
    };

    /**
     * PDF_EXTRACTOR_FILENAME
     */
    public final static String PDFEXTRACTOR_EXECUTABLE_FILENAME_1 = "pdftool";
    public final static String PDFEXTRACTOR_EXECUTABLE_FILENAME_2 = "pdf2svg";
    public final static String PDFEXTRACTOR_EXECUTABLE_WORKDIR_NAME = "oxdcc.tmp";

    /**
     * Initializes a new {@link ClientConfig}.
     * @param configService
     */
    public ClientConfig(@NonNull final ConfigurationService configService) {
        implReadConfig(configService);
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    public void reloadConfiguration(ConfigurationService configService) {
        if (DocumentConverterManager.isLogTrace()) {
            DocumentConverterManager.logTrace("DC client is reloading config items: " + Arrays.toString(RELOADABLE_CONFIGITEMS));
        }

        implReadConfig(configService, RELOADABLE_CONFIGITEMS);
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    public Interests getInterests() {
        return Reloadables.interestsForProperties(RELOADABLE_CONFIGITEMS);
    }


    // DocumentConverter Client configuration properties -----------------------

    /**
     * Server Base URL to be used for remote DocumentConverter conversions;
     * if left empty, no document conversions will be handled
     */
    public URL REMOTEURL_DOCUMENTCONVERTER = null;

    // TODO: load cacheUrl from config...
    /**
     * Cache URL(s) to be used for remote conversions; the local backend will handle caching in every case, but a cache speedup can be
     * achieved by adding additional cache servers
     */
    public URL CACHESERVICE_URL = null;

    /**
     * the maximum size in Bytes for documents to be converted;
     * -1 for no upper limit; default is 128MB
     */
    public long MAX_DOCUMENT_SOURCEFILESIZE = 134217728;

    public boolean PREVIEW_ENABLE_CACHE_LOOKUP = true;

    /**
     * the list of supported extension types
     * or an empty set, if no extension
     * check should be performed
     */
    public Set<String> SUPPORTED_TYPES = new HashSet<>();

    /**
     * The path to the directory, containing the PDF
     * extraction executable working directory.
     */
    public File PDFEXTRACTOR_WORKDIR = new File("/tmp", PDFEXTRACTOR_EXECUTABLE_WORKDIR_NAME);

    /**
     * The maximum virtual memory in MegaBytes that an externally
     * started PDF extractor process is allowed to allocate
     */
    public int PDFEXTRACTOR_MAX_VMEM_MB = 1024;


    /**
     * The path to the directory, containing the PDF
     * extraction executable.
     */
    public File PDFEXTRACTOR_EXECUTABLE_PATH = new File("/opt/open-xchange/sbin", PDFEXTRACTOR_EXECUTABLE_FILENAME_1);

    /**
     * The auto cleanup time in milliseconds after which
     * a stateful conversion is terminated
     */
    public int AUTO_CLEANUP_TIMEOUT_MILLIS = 0;

    // - Internally used properties --------------------------------------------

    /**
     * the location of the log
     */
    public String CLIENT_LOGFILE = null;

    /**
     * determines the amount and detail of logging data; 3 = errors, 2 = errors and warnings, 1 = errors and warnings and infos, 0 =
     * disabled
     */
    public String CLIENT_LOGLEVEL = null;

    /**
     * Internal flag to enable the debug mode
     */
    public boolean CLIENT_DEBUG = false;

    // - Implementation --------------------------------------------------------

    void implReadConfig(ConfigurationService configService, final String... configPropertyNames) {
        final String emptyString = "";
        final int emptyInt = Integer.MIN_VALUE;
        final HashSet<String> itemsToRead = new HashSet<>();
        String curStringEntry = emptyString;
        int curIntEntry = emptyInt;

        itemsToRead.addAll(
                Arrays.asList(ArrayUtils.isEmpty(configPropertyNames) ?
                    ALL_CONFIGITEMS :
                        configPropertyNames));

        // REMOTEURL_DOCUMENTCONVERTER
        if (itemsToRead.contains(CONFIGITEM_REMOTE_DOCUMENTCONVERTER_URL)) {
            REMOTEURL_DOCUMENTCONVERTER = null;

            if (isNotBlank(curStringEntry = configService.getProperty(CONFIGITEM_REMOTE_DOCUMENTCONVERTER_URL, emptyString))) {
                try {
                    if (curStringEntry.startsWith("http")) {
                        REMOTEURL_DOCUMENTCONVERTER = new URL(curStringEntry);
                    }
                } catch (final Exception e) {
                    DocumentConverterManager.logError("DC client com.openexchange.documentconverter.client.remoteDocumentConverterUrl is not a valid http URL: " + curStringEntry + " => Remote document conversion disabled!: " + e.getMessage());
                }
            }

            if (null == REMOTEURL_DOCUMENTCONVERTER) {
                DocumentConverterManager.logError("DC client com.openexchange.documentconverter.client.remoteDocumentConverterUrl is not set at all => Documentconverter functionality will not be available!");
            }
        }

        // CACHESERVICE_URL
        if (itemsToRead.contains(CONFIGITEM_CACHESERVICE_URL)) {
            CACHESERVICE_URL = null;

            if (isNotBlank(curStringEntry = configService.getProperty(CONFIGITEM_CACHESERVICE_URL, emptyString))) {
                try {
                    if (curStringEntry.startsWith("http")) {
                        CACHESERVICE_URL = new URL(curStringEntry);
                    }
                } catch (final Exception e) {
                    DocumentConverterManager.logError("DC client com.openexchange.documentconverter.client.cacheServiceUrl is not a valid http URL: " + e.getMessage());
                }
            }

            if (null == CACHESERVICE_URL) {
                DocumentConverterManager.logError("DC client com.openexchange.documentconverter.client.cacheServiceUrl is not set");
            }
        }

        // MAX_DOCUMENT_SOURCEFILESIZE_MB
        if (itemsToRead.contains(CONFIGITEM_MAX_DOCUMENT_SOURCEFILE_SIZE_MB) &&
            (curIntEntry = configService.getIntProperty(CONFIGITEM_MAX_DOCUMENT_SOURCEFILE_SIZE_MB, emptyInt)) != emptyInt) {

            MAX_DOCUMENT_SOURCEFILESIZE = (curIntEntry > 0) ? (curIntEntry * 1024 * 1024) : curIntEntry;
        }

        // PREVIEW_ENABLE_CACHE_LOOKUP
        if (itemsToRead.contains(CONFIGITEM_ENABLE_CACHE_LOOKUP)) {
            PREVIEW_ENABLE_CACHE_LOOKUP = configService.getBoolProperty(CONFIGITEM_ENABLE_CACHE_LOOKUP, false);
        }

        // SUPPORTED_TYPES
        if (itemsToRead.contains(CONFIGITEM_SUPPORTED_TYPES)) {
            final List<String> configList = configService.getProperty(CONFIGITEM_SUPPORTED_TYPES, "", ",");

            if ((null != configList) && configList.size() > 0) {
                for (final String curEntry : configList) {
                    SUPPORTED_TYPES.add(curEntry.toLowerCase());
                }
            }
        }

        // PDFEXTRACTOR_EXECUTABLE_PATH
        if (itemsToRead.contains(CONFIGITEM_PDFEXTRACTOR_INSTALL_DIR) &&
            isNotBlank(curStringEntry = configService.getProperty(CONFIGITEM_PDFEXTRACTOR_INSTALL_DIR, emptyString))) {

            File pdfExtractorExecutable = null;

            // check all possible binary names and use executable one
            if ((pdfExtractorExecutable = new File(curStringEntry, PDFEXTRACTOR_EXECUTABLE_FILENAME_1)).canExecute() ||
                (pdfExtractorExecutable = new File(curStringEntry, PDFEXTRACTOR_EXECUTABLE_FILENAME_2)).canExecute()) {

                PDFEXTRACTOR_EXECUTABLE_PATH = pdfExtractorExecutable;
            } else {
                DocumentConverterManager.logError("DC client PDF extraction tool cannot be executed in path: " +
                    pdfExtractorExecutable.getParent() +
                    " => PDF Page extraction will be disabled!");
            }
        }

        // PDFEXTRACTOR_WORKDIR
        if (itemsToRead.contains(CONFIGITEM_PDFEXTRACTOR_WORK_DIR)) {
            curStringEntry = configService.getProperty(CONFIGITEM_PDFEXTRACTOR_WORK_DIR);

            if (isBlank(curStringEntry)) {
                curStringEntry = configService.getProperty("UPLOAD_DIRECTORY");

                if (isBlank(curStringEntry)) {
                    curStringEntry = FileUtils.getTempDirectoryPath();
                }
            }

            final File pdfExtractorWorkDir = new File(curStringEntry, PDFEXTRACTOR_EXECUTABLE_WORKDIR_NAME);

            FileUtils.deleteQuietly(pdfExtractorWorkDir);

            if (DocumentConverterManager.validateOrMkdir(pdfExtractorWorkDir)) {
                PDFEXTRACTOR_WORKDIR = pdfExtractorWorkDir;
            } else {
                DocumentConverterManager.logError("DC client PDF extraction tool working directoy cannot be created or is not writable: " +
                    pdfExtractorWorkDir.getPath() +
                    " => PDF Page extraction will be disabled!");

                PDFEXTRACTOR_EXECUTABLE_PATH = null;
                PDFEXTRACTOR_WORKDIR = null;
            }
        }

        // CONFIGITEM_PDFEXTRACTOR_MAX_VMEM_MB
        if (itemsToRead.contains(CONFIGITEM_PDFEXTRACTOR_MAX_VMEM_MB) &&
            (curIntEntry = configService.getIntProperty(CONFIGITEM_PDFEXTRACTOR_MAX_VMEM_MB, emptyInt)) != emptyInt) {

            PDFEXTRACTOR_MAX_VMEM_MB = Math.max(curIntEntry, -1);
        }

        // CONFIGITEM_AUTO_CLEANUP_TIMEOUT_SECONDS
        if (itemsToRead.contains(CONFIGITEM_AUTO_CLEANUP_TIMEOUT_SECONDS) &&
            (curIntEntry = configService.getIntProperty(CONFIGITEM_AUTO_CLEANUP_TIMEOUT_SECONDS, emptyInt)) != emptyInt) {

            AUTO_CLEANUP_TIMEOUT_MILLIS = Math.max(curIntEntry, 0) * 1000;
        }

        // - Internally used config items ----------------------------------

        // ENGINE_LOGFILE
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.client.logFile", emptyString)) != emptyString) {
            CLIENT_LOGFILE = curStringEntry;
        }

        // ENGINE_LOGLEVEL
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.client.logLevel", emptyString)) != emptyString) {
            CLIENT_LOGLEVEL = curStringEntry;
        }

        // ENGINE_DEBUG
        CLIENT_DEBUG = configService.getBoolProperty("com.openexchange.documentconverter.client.debug", false);

    }
}
