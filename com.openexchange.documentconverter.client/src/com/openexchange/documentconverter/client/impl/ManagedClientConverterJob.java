/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import com.openexchange.documentconverter.DocumentConverterClientMetrics;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.DocumentInformation;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;

/**
 * {@link ManagedClientConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
public class ManagedClientConverterJob {

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ManagedRemoteJob#convert(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    public static InputStream process(String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties) {
        InputStream ret = null;
        if (implCheckJobPreconditions(jobProperties, resultProperties, 1)) {
            if (implPrepareRemoteCall(jobProperties, RCALL_CONVERT)) {
                jobProperties.put(Properties.PROP_JOBTYPE, jobType);
                if (null != m_remoteUrl) {
                    URL remoteUrl = null;
                    try {
                        remoteUrl = new URL(m_remoteUrl.toString());
                    } catch (final MalformedURLException e) {
                        DocumentConverterManager.logExcp(e);
                        return ret;
                    }

                    try {
                        final HttpPost uploadFile = new HttpPost(remoteUrl.toString());
                        try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                            final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create().setCharset(Charset.forName("UTF-8"))
                                .addPart("sourcefile", new FileBody((File)jobProperties.get(Properties.PROP_INPUT_FILE), ContentType.APPLICATION_OCTET_STREAM, "ByteArray.bin"));
                            implAddRemoteParams(multipartEntityBuilder, jobProperties);
                            final HttpEntity reqEntity = multipartEntityBuilder.build();

                            uploadFile.setEntity(reqEntity);
                            try(CloseableHttpResponse response = httpClient.execute(uploadFile)) {
                                final HttpEntity responseEntity = response.getEntity();
                                final String contentType = responseEntity.getContentType() != null ? responseEntity.getContentType().getValue() : null;

                                resultProperties.clear();
                                if ("application/json".equals(contentType)) {
                                    Properties.fromJSONResult(JSONObject.parse(responseEntity.getContent()).toObject(), resultProperties);
                                }
                                else {
                                    resultProperties.put(Properties.PROP_RESULT_BUFFER, IOUtils.toByteArray(responseEntity.getContent()));
                                }

                                m_clientManager.getDocumentConverterClientMetrics().
                                incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, null);
                            }
                            catch (final Exception e) {
                                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));

                                m_clientManager.getDocumentConverterClientMetrics().
                                    incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, e);

                                DocumentConverterManager.logExcp(e);
                            }
                        }
                        final byte[] resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);
                        if (null != resultBuffer) {
                            ret = new ByteArrayInputStream(resultBuffer);
                        }
                    } catch (Exception e) {
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));

                        DocumentConverterManager.logExcp("DC client received exception during remote call", e);

                        m_clientManager.getDocumentConverterClientMetrics().
                            incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, e);

                        m_clientManager.resetRemoteConnectionDC();
                    }
                }
            }
        }
        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    public static byte[] implGetInputBuffer(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        byte[] ret = null;

        try (final InputStream inputStm = (jobProperties.containsKey(Properties.PROP_INPUT_FILE) ?
            new FileInputStream((File) jobProperties.get(com.openexchange.documentconverter.Properties.PROP_INPUT_FILE)) :
                (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM))) {

            if (null != inputStm) {
                ret = IOUtils.toByteArray(inputStm);
            }
        } catch (final Exception e) {
            DocumentConverterManager.logExcp(e);
        }

        // check, if length of byte array is larger than specified config source file size
        if ((null != ret) && (implSetSourceFileSizeError(m_clientManager, ret.length, jobProperties, resultProperties))) {
            ret = null;
        }

        return ret;
    }

    /**
     * @param jobProperties
     */
    /**
     * @param jobProperties
     * @param resultProperties
     * @param minRemoteAPIVersion
     * @return true, if job processing can proceed
     */
    public static boolean implCheckJobPreconditions(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties, int minRemoteAPIVersion) {
        boolean ret = false;

        if (null != jobProperties) {
            // job is valid by default, if job properties exists at all
            // and source file size fits into the given constraints
            ret = CheckJobInputFileSize(m_clientManager, jobProperties, resultProperties) &&
                CheckSupportedJobInputType(m_clientManager, jobProperties, resultProperties);
        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        if (ret && (null != m_remoteUrl)) {
            if (!(ret = m_clientManager.getRemoteAPIVersionDC(false) >= minRemoteAPIVersion)) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));
            }
        }

        if (!ret) {
            final JobErrorEx jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
            final String fileName = (null != jobProperties) ? ((String) jobProperties.get(Properties.PROP_INFO_FILENAME)) : null;

            DocumentConverterManager.logWarn("DC client remote conversion failed",
                new LogData("joberror", jobErrorEx.getJobError().getErrorText()),
                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param resultProperties
     * @return true, if source file size is unlimited or below/equal
     *  to the given maximum source file size
     */
    public static boolean CheckJobInputFileSize(ClientManager clientManager, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if (null != jobProperties) {
            // check max source file size, if source file is given
            final File inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

            if (null != inputFile) {
                // check maxSourceFile
                ret = !implSetSourceFileSizeError(clientManager, inputFile.length(), jobProperties, resultProperties);
            } else {
                // job is valid by default, if job properties exists
                // at all and source file is given as input stream
                ret = true;
            }
        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param resultProperties
     * @return true, if source file size is unlimited or below/equal
     *  to the given maximum source file size
     */
    public static boolean CheckSupportedJobInputType(ClientManager clientManager, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if (null != jobProperties) {
            // check max source file size, if source file is given
            String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);

            if (StringUtils.isEmpty(inputType)) {
                final String fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

                if (StringUtils.isNotEmpty(fileName)) {
                    inputType = FilenameUtils.getExtension(fileName);

                    if (StringUtils.isNotEmpty(inputType)) {
                        inputType = inputType.trim().toLowerCase();
                    }
                }
            }

            if (StringUtils.isEmpty(inputType)) {
                inputType = "bin";
            }

            ret = !implSetSupportedTypeError(clientManager, inputType, jobProperties, resultProperties);
        }

        return ret;
    }

    /**
     * @param sourceSize
     * @param maxSize
     * @param resultProperties
     * @return true, if source size is larger than max size
     */
    private static boolean implSetSourceFileSizeError(ClientManager clientManager, long sourceSize, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        final long maxSize = clientManager.getClientConfig().MAX_DOCUMENT_SOURCEFILESIZE;
        boolean ret = false;

        if (sourceSize > maxSize) {
            ret = true;

            if (null != resultProperties) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.MAX_SOURCESIZE.getErrorCode()));

                if (DocumentConverterManager.isLogError()) {
                    DocumentConverterManager.logError("DC client detected source file size larger than configured size",
                        jobProperties,
                        new LogData("sourcesize", Long.valueOf(sourceSize).toString()),
                        new LogData("maxsize", Long.valueOf(maxSize).toString()));
                }
            }
        }
        return ret;
    }

    /**
     * @param sourceSize
     * @param maxSize
     * @param resultProperties
     * @return true, if source size is larger than max size
     */
    private static boolean implSetSupportedTypeError(ClientManager clientManager, String inputType, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        final Set<String> supportedTypes = clientManager.getClientConfig().SUPPORTED_TYPES;
        final boolean ret = (supportedTypes.size() > 0) &&
            DocumentInformation.isExtensionSupported(inputType) &&
            !supportedTypes.contains(inputType);

        if (ret) {
            if (null != resultProperties) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.UNSUPPORTED.getErrorCode()));
            }

            if (DocumentConverterManager.isLogError()) {
                DocumentConverterManager.logError("DC client detected unsupported input file extension",
                    jobProperties, new LogData("inputype", inputType));
            }
        }
        return ret;
    }

    /**
     * @param jobProperties
     * @param method
     * @return
     */
    public static boolean implPrepareRemoteCall(HashMap<String, Object> jobProperties, String method) {
        final String calledRemoteUrlString = (String) jobProperties.get(Properties.PROP_ENGINE_REMOTEURL);
        URL calledRemoteUrl = null;
        boolean ret = false;

        if (!StringUtils.isEmpty(calledRemoteUrlString)) {
            try {
                calledRemoteUrl = new URL(calledRemoteUrlString);
            } catch (final MalformedURLException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        // check if a remote conversion is requested and if we're not remotely called by ourselves
        if ((null != m_remoteUrl) && ((null == calledRemoteUrl) || !calledRemoteUrl.equals(m_remoteUrl))) {
            jobProperties.put(Properties.PROP_REMOTE_METHOD, method);
            jobProperties.put(Properties.PROP_ENGINE_REMOTEURL, m_remoteUrl.toString());
            ret = true;
        }

        // get stored cookie from previous beginPage call and add to job properties, if possible
        if (method.equals(RCALL_GETCONVERSIONPAGE) || method.equals(RCALL_ENDPAGECONVERSION)) {
            final String converterJobId = (String) jobProperties.get(Properties.PROP_JOBID);

            if (StringUtils.isNotEmpty(converterJobId)) {
                final String converterCookie = m_converterCookieMap.get(converterJobId);

                if (StringUtils.isNotEmpty(converterCookie)) {
                    jobProperties.put(Properties.PROP_CONVERTER_COOKIE, converterCookie);
                }
            }
        }
        return ret;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @return
     */
    private static void implAddRemoteParam(@NonNull final MultipartEntityBuilder multipartEntityBuilder, @NonNull String key, Object param) {
        if (param != null) {
            multipartEntityBuilder.addTextBody(key, param.toString(), ContentType.TEXT_PLAIN);
        }
    }

    private static void implAddRemoteParamURL(@NonNull final MultipartEntityBuilder multipartEntityBuilder, @NonNull String key, Object urlParam) {
        if (urlParam != null) {
            try {
                implAddRemoteParam(multipartEntityBuilder, key, URLEncoder.encode((String)urlParam, "UTF-8"));
            } catch (final UnsupportedEncodingException e) {
                DocumentConverterManager.logExcp(e);
            }
        }
    }

    private static MultipartEntityBuilder implAddRemoteParams(@NonNull final MultipartEntityBuilder multipartEntityBuilder, @NonNull final HashMap<String, Object> jobProperties) {
        implAddRemoteParam(multipartEntityBuilder, "method", jobProperties.get(Properties.PROP_REMOTE_METHOD));
        implAddRemoteParam(multipartEntityBuilder, "jobType", jobProperties.get(Properties.PROP_JOBTYPE));
        implAddRemoteParam(multipartEntityBuilder, "jobId", jobProperties.get(Properties.PROP_JOBID));
        implAddRemoteParam(multipartEntityBuilder, "priority", jobProperties.get(Properties.PROP_PRIORITY));
        implAddRemoteParam(multipartEntityBuilder, "locale", jobProperties.get(Properties.PROP_LOCALE));
        implAddRemoteParam(multipartEntityBuilder, "cacheHash", jobProperties.get(Properties.PROP_CACHE_HASH));
        implAddRemoteParam(multipartEntityBuilder, "filterShortName", jobProperties.get(Properties.PROP_FILTER_SHORT_NAME));
        implAddRemoteParam(multipartEntityBuilder, "inputType", jobProperties.get(Properties.PROP_INPUT_TYPE));
        implAddRemoteParamURL(multipartEntityBuilder, "inputUrl", jobProperties.get(Properties.PROP_INPUT_URL));
        // TODO: not implemented in server
        // implAddRemoteParam(multipartEntityBuilder, "pixelx", jobProperties.get(Properties.PROP_PIXEL_X));
        // implAddRemoteParam(multipartEntityBuilder, "pixely", jobProperties.get(Properties.PROP_PIXEL_Y));
        implAddRemoteParam(multipartEntityBuilder, "pixelWidth", jobProperties.get(Properties.PROP_PIXEL_WIDTH));
        implAddRemoteParam(multipartEntityBuilder, "pixelHeight", jobProperties.get(Properties.PROP_PIXEL_HEIGHT));
        implAddRemoteParam(multipartEntityBuilder, "mimeType", jobProperties.get(Properties.PROP_MIME_TYPE));
        implAddRemoteParam(multipartEntityBuilder, "pageRange", jobProperties.get(Properties.PROP_PAGE_RANGE));
        implAddRemoteParam(multipartEntityBuilder, "pageNumber", jobProperties.get(Properties.PROP_PAGE_NUMBER));
        implAddRemoteParam(multipartEntityBuilder, "shapeNumber", jobProperties.get(Properties.PROP_SHAPE_NUMBER));
        implAddRemoteParam(multipartEntityBuilder, "zipArchive", jobProperties.get(Properties.PROP_ZIP_ARCHIVE));
        implAddRemoteParam(multipartEntityBuilder, "featuresId", jobProperties.get(Properties.PROP_FEATURES_ID));
        implAddRemoteParam(multipartEntityBuilder, "cacheOnly", jobProperties.get(Properties.PROP_CACHE_ONLY));
        implAddRemoteParam(multipartEntityBuilder, "noCache", jobProperties.get(Properties.PROP_NO_CACHE));
        implAddRemoteParam(multipartEntityBuilder, "hideChanges", jobProperties.get(Properties.PROP_HIDE_CHANGES));
        implAddRemoteParam(multipartEntityBuilder, "hideComments", jobProperties.get(Properties.PROP_HIDE_COMMENTS));
        implAddRemoteParam(multipartEntityBuilder, "async", jobProperties.get(Properties.PROP_ASYNC));
        implAddRemoteParam(multipartEntityBuilder, "imageResolution", jobProperties.get(Properties.PROP_IMAGE_RESOLUTION));
        implAddRemoteParam(multipartEntityBuilder, "imageScaleType", jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE));
        implAddRemoteParamURL(multipartEntityBuilder, "infoFileName", jobProperties.get(Properties.PROP_INFO_FILENAME));
        implAddRemoteParam(multipartEntityBuilder, "userRequest", jobProperties.get(Properties.PROP_USER_REQUEST));
        implAddRemoteParamURL(multipartEntityBuilder, "shapeReplacements", jobProperties.get(Properties.PROP_SHAPE_REPLACEMENTS));
        return multipartEntityBuilder;
    }

    // - Static members ---------------------------------------------------------

    protected static final ClientManager m_clientManager = ClientManager.get();

    protected static final URL m_remoteUrl = ClientManager.get().getClientConfig().REMOTEURL_DOCUMENTCONVERTER;

    protected static final String ZIP_MIMETYPE = "application/zip";

    protected static final String BASE64_PATTERN = "base64,";

    protected static final String RCALL_CONVERT = "convert";

    protected static final String RCALL_QUERY_AVAILABILITY = "queryavailability";

    protected static final String RCALL_BEGINPAGECONVERSION = "beginpageconversion";

    protected static final String RCALL_GETCONVERSIONPAGE = "getconversionpage";

    protected static final String RCALL_ENDPAGECONVERSION = "endpageconversion";

    protected static Map<String, String> m_converterCookieMap = new ConcurrentHashMap<>();
}
