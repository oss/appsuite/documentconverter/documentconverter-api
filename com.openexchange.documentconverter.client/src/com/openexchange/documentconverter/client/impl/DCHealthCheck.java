package com.openexchange.documentconverter.client.impl;

import java.net.URL;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.NonNull;

/**
 * {@link DCHealthCheck}
 *
 * @since v8.0.0
 */
public class DCHealthCheck implements HealthCheck {

    /**
     * DC_CLIENT_HEALTH_CHECK
     */
    final private static String DC_CLIENT_HEALTH_CHECK = "DocumentConverterClientHealthCheck";

    /**
     * DC_CLIENT_REMOTEURL
     */
    final private static String DC_CLIENT_REMOTEURL = "remoteDocumentConverterUrl";

    /**
     * DC_SERVICE_AVAILABLE
     */
    final private static String DC_SERVICE_AVAILABLE = "serviceAvailable";

    /**
     * Initializes a new {@link DCHealthCheck}.
     * @param client
     */
    public DCHealthCheck(@NonNull ClientManager clientManager) {
        m_clientManager = clientManager;
    }

    /**
     *
     */
    @Override
    public HealthCheckResponse call() {
        final URL remoteURL = m_clientManager.getClientConfig().REMOTEURL_DOCUMENTCONVERTER;
        final HealthCheckResponseBuilder healthResponseBuilder = HealthCheckResponse.builder().
            name(DC_CLIENT_HEALTH_CHECK).
            withData(DC_CLIENT_REMOTEURL, (null != remoteURL) ? remoteURL.toString() : DocumentConverterUtil.STR_NOT_AVAILABLE).
            withData(DC_SERVICE_AVAILABLE, m_clientManager.getRemoteAPIVersionDC(true) >= 1);

        // UP/DOWN status is set to UP in every case!
        healthResponseBuilder.up();

        return healthResponseBuilder.build();
    }

    // - Members ---------------------------------------------------------------

    @NonNull final private ClientManager m_clientManager;
}
