/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.DocumentConverterClientMetrics;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.HttpHelper;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;

/**
 * {@link RemoteDocumentConverterCheck}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
class RemoteValidator {

    final private static long LOG_TIMEOUT = 60000;

    protected class RemoteValidatorCallable implements Callable<Integer> {

        /**
         *
         */
        public RemoteValidatorCallable() {
            super();
        }

        /**
         * @return
         * @throws Exception
         */
        @Override
        public Integer call() throws Exception {
            final StringBuilder serverIdBuilder = new StringBuilder(32);
            final int oldRemoteApiVersion = m_remoteAPIVersion.get();
            final AtomicInteger remoteApiVersion = new AtomicInteger(-1);
            final long curTimeMillis = System.currentTimeMillis();
            int newRemoteApiversion = oldRemoteApiVersion;

            // check remote connection API; may be checked repeatedly, if a valid remote URL is set
            if (null != m_remoteUrl) {
                try {
                    HttpHelper.get().executeGet(
                        m_remoteUrl,
                        (int responseCode, InputStream responseInputStream, String resultCookie) -> {
                            try (final BufferedReader inputReader = new BufferedReader(new InputStreamReader(responseInputStream))) {
                                for (String readLine = null; (readLine = inputReader.readLine()) != null;) {
                                    readLine = readLine.toLowerCase();

                                    // check, if we really have a DC server connection first
                                    // newer versions have the serverId set => try to extract this first
                                    if (readLine.contains("id:")) {
                                        final Matcher matcher = serverIDPattern.matcher(readLine);

                                        if (matcher.find()) {
                                            serverIdBuilder.append(matcher.group(1));
                                        }
                                    }

                                    if (readLine.contains("api:")) {
                                        final Matcher matcher = apiVersionPattern.matcher(readLine);

                                        if (matcher.find()) {
                                            remoteApiVersion.set(Integer.parseInt(matcher.group(1)));
                                        }
                                    }
                                }

                                implIncrementConnectionStatusCount(null);
                            } catch (Exception e) {
                                implIncrementConnectionStatusCount(e);

                                if (implIsConnectionErrorLoggingAllowed(curTimeMillis, false)) {
                                    DocumentConverterManager.logExcp("DC client received exception during check for remote server connection", e);
                                }

                            }
                        });
                } catch (Exception e) {
                    implIncrementConnectionStatusCount(e);

                    if (implIsConnectionErrorLoggingAllowed(curTimeMillis, false)) {
                        DocumentConverterManager.logExcp("DC client received exception during check for remote server connection", e);
                    }
                }

                newRemoteApiversion = remoteApiVersion.get();

                if (newRemoteApiversion < 0) {
                    // no connection established until now => log and reset last log timestamp
                    if (implIsConnectionErrorLoggingAllowed(curTimeMillis, true)) {
                        DocumentConverterManager.logError("DC client remote connection could not be established => please check remote converter setup",
                            new LogData("remoteurl", m_remoteUrl.toString()),
                            new LogData("async_request", "true"));
                    }
                } else if (oldRemoteApiVersion != newRemoteApiversion) {
                    // connection established
                    DocumentConverterManager.logInfo("DC client established remote connection",
                        new LogData("remoteurl", m_remoteUrl.toString()),
                        new LogData("async_request", "true"));

                }

                if (m_connectionType.getServerId() != null) {
                    final String serverId = serverIdBuilder.toString();
                    if (!m_connectionType.getServerId().equals(serverId)) {
                        if (serverId.isEmpty()) {
                            DocumentConverterManager.logWarn("DC client remote connection " + m_remoteUrl + " does not have a serverId");
                        }
                        else {
                            DocumentConverterManager.logWarn("DC client remote connection, the serverId " + serverId + " from " + m_remoteUrl + " does not match " + m_connectionType.getServerId());
                        }
                    }
                }
            }
            return newRemoteApiversion;
        }
    }

    /**
     * Initializes a new {@link RemoteValidator}.
     * @param serverType
     * @param remoteURL
     */
    RemoteValidator(@NonNull final DocumentConverterClientMetrics clientMetrics, @NonNull DocumentConverterClientMetrics.ConnectionType connectionType, @NonNull final URL remoteURL) {
        super();

        m_clientMetrics = clientMetrics;
        m_connectionType = connectionType;
        implSetRemoteURL(remoteURL);

    }

    /**
     * @param remoteURL
     */
    void updateRemoteURL(@NonNull final URL remoteURL) {
        implSetRemoteURL(remoteURL);
    }

    /**
     * @return
     */
    boolean isRemoteServerSupported() {
        return (null != m_remoteUrl);
    }

    /**
     * @return
     */
    int getRemoteAPIVersion(final boolean force) {
        final int oldRemoteApiVersion = m_remoteAPIVersion.get();

        if (force || (oldRemoteApiVersion <= 1)) {
            if ((null != m_callable) && m_isRunning.compareAndSet(false, true)) {
                final Future<Integer> resultFuture = Executors.newSingleThreadExecutor().submit(m_callable);

                if (null != resultFuture) {
                    try {
                        final int newRemoteApiVersion = resultFuture.get().intValue();

                        m_remoteAPIVersion.set(newRemoteApiVersion);

                        return newRemoteApiVersion;
                    } catch (Exception e) {
                        DocumentConverterManager.logError("DC client remote validator caught exception: " + Throwables.getRootCause(e));
                    } finally {
                        m_isRunning.set(false);
                    }
                }
            }
        }

        return oldRemoteApiVersion;
    }

    /**
     *
     */
    synchronized void resetConnection() {
        if ((m_remoteAPIVersion.get() >= 1) && !m_isRunning.get()) {
            m_remoteAPIVersion.set(-1);

            if (DocumentConverterManager.isLogError()) {
                DocumentConverterManager.logError("DC client lost remote connection:", new LogData("remoteurl", m_remoteUrl.toString()));
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param remoteUrl
     */
    void implSetRemoteURL(@NonNull final URL remoteUrl) {
        if ((null == m_remoteUrl) || !remoteUrl.equals(m_remoteUrl)) {
            m_remoteAPIVersion.set(-1);
            m_isRunning.set(false);
            m_remoteUrl = remoteUrl;

            // trigger new connection validation
            getRemoteAPIVersion(true);
        }
    }

    /**
     * @param curTimeMillis
     * @param resetLastLogTime
     * @return
     */
    boolean implIsConnectionErrorLoggingAllowed(final long curTimeMillis, final boolean resetLastLogTime) {
        final long lastLogTimeMillis = m_lastLogTimeMillis.get();
        final boolean errorLoggingAllowed = ((0 == lastLogTimeMillis) || ((curTimeMillis - lastLogTimeMillis) >= LOG_TIMEOUT));

        if (resetLastLogTime) {
            m_lastLogTimeMillis.set(curTimeMillis);
        }

        return errorLoggingAllowed;
    }

    /**
     *
     */
    void implIncrementConnectionStatusCount(@Nullable final Exception e) {
        m_clientMetrics.incrementConnectionStatusCount(m_connectionType, e);
    }

    // - Static members --------------------------------------------------------

    protected static Pattern apiVersionPattern = Pattern.compile("api\\:\\s*v([0-9]+)");
    protected static Pattern serverIDPattern = Pattern.compile("id\\:\\s*(([0-9a-f]+\\-){4}[0-9a-f]+)");

    // - Members ---------------------------------------------------------------

    final protected DocumentConverterClientMetrics m_clientMetrics;

    final protected DocumentConverterClientMetrics.ConnectionType m_connectionType;

    protected RemoteValidatorCallable m_callable = new RemoteValidatorCallable();

    protected AtomicInteger m_remoteAPIVersion = new AtomicInteger(-1);

    protected AtomicBoolean m_isRunning = new AtomicBoolean(false);

    protected URL m_remoteUrl = null;

    protected AtomicLong m_lastLogTimeMillis = new AtomicLong(0);
}
