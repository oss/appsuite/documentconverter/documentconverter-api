/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.documentconverter.DocumentConverterClientMetrics;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.FileHashBuilder;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.PDFExtractor;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.SingletonService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link ClientManager}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
@SingletonService
public class ClientManager extends DocumentConverterManager implements IDocumentConverter, Reloadable {

    final private static String DC_CLIENT_PDF_EXTRACTOR_LOG_PREFIX = "DC client PDFExtractor";

    final private static int ASYNC_QUEUE_LENTGH = 2048;

    /**
     * @param services
     * @param config
     * @return
     */
    @NonNull public static synchronized ClientManager newInstance(ClientConfig config, ServiceLookup serviceLookup) {
        if (null == m_clientManager) {
            m_clientManager = new ClientManager(config, serviceLookup);
        }

        return m_clientManager;
    }

    /**
     * @return
     */
    @Nullable public static synchronized ClientManager get() {
        return m_clientManager;
    }

    /**
     * Initializes a new {@link ClientManager}.
     */
    private ClientManager(@NonNull final ClientConfig clientConfig, ServiceLookup serviceLookup) {
        super(clientConfig.PDFEXTRACTOR_WORKDIR);

        m_clientConfig = clientConfig;
        m_serviceLookup = serviceLookup;

        m_asyncExecutor = new AsyncExecutor(this, this, ASYNC_CONVERTER_THREADCOUNT, ASYNC_QUEUE_LENTGH);
        m_remoteValidator_DC = new RemoteValidator(m_clientMetrics, DocumentConverterClientMetrics.ConnectionType.VALIDATION_DC, clientConfig.REMOTEURL_DOCUMENTCONVERTER);
        m_remoteValidator_CS = new RemoteValidator(m_clientMetrics, DocumentConverterClientMetrics.ConnectionType.VALIDATION_CS, clientConfig.CACHESERVICE_URL);

        try {
            m_pdfExtractor = new PDFExtractor(this, clientConfig.PDFEXTRACTOR_EXECUTABLE_PATH, clientConfig.PDFEXTRACTOR_MAX_VMEM_MB) {
                @Override
                public String getLogPrefix() {
                    return DC_CLIENT_PDF_EXTRACTOR_LOG_PREFIX;
                }
            };
        } catch (@SuppressWarnings("unused") IOException e) {
            logError("DC client PDFExtractor could not be initiaized correctly: " + clientConfig.PDFEXTRACTOR_EXECUTABLE_PATH);
        }

        // set cache parameters
        final CacheDescriptor cacheDescriptor = new CacheDescriptor();
        cacheDescriptor.cacheServiceUrl = clientConfig.CACHESERVICE_URL != null ? clientConfig.CACHESERVICE_URL.toString() : null;
        try {
            m_cacheServiceClient = new CacheServiceClient(cacheDescriptor);
        } catch (Exception e) {
            logError("DC client CacheServiceClient could not be initiaized correctly");
        }

        // check for invalid remote URL
        if ((null == clientConfig.REMOTEURL_DOCUMENTCONVERTER) || (clientConfig.REMOTEURL_DOCUMENTCONVERTER.toString().length() <= 0)) {
            logError("DC client remoteDocumentConverterUrl property is empty or not set at all. Client is not able to perform conversions until a valid Url is set!");
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        if (null != m_clientConfig) {
            m_clientConfig.reloadConfiguration(configService);

            m_remoteValidator_DC.updateRemoteURL(m_clientConfig.REMOTEURL_DOCUMENTCONVERTER);
            m_remoteValidator_CS.updateRemoteURL(m_clientConfig.CACHESERVICE_URL);

            // check for invalid remote URL
            if ((null == m_clientConfig.REMOTEURL_DOCUMENTCONVERTER) || (m_clientConfig.REMOTEURL_DOCUMENTCONVERTER.toString().length() <= 0)) {
                logError("DC client remoteDocumentConverterUrl property is empty or not set at all. Client is not able to perform conversions until a valid Url is set!");
            }

            // check for invalid remote URL
            if ((null == m_clientConfig.CACHESERVICE_URL) || (m_clientConfig.CACHESERVICE_URL.toString().length() <= 0)) {
                logError("DC client cacheServiceUrl property is empty or not set at all. Client is not able to perform cache operations until a valid Url is set!");
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    @Override
    public Interests getInterests() {
        return (null != m_clientConfig) ?
                m_clientConfig.getInterests() :
                    null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IClientManager#convert(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public InputStream convert(String jobType, @NonNull HashMap<String, Object> _jobProperties, @NonNull HashMap<String, Object> resultProperties, Session session) {

        if (jobType == null) {
            logError("DC client convert, jobType is required");
            return null;
        }

        final HashMap<String, Object> jobProperties = new HashMap<>(_jobProperties.size());
        jobProperties.putAll(_jobProperties);

        // always and only work on file based job properties here, since
        // we may need to call two subsequent conversions with job
        // properties, that are read more than once, which is problematic
        // in case of an InputStream based source content property
        // ... creating file based job properties
        final MutableWrapper<Boolean> deleteInputFile = new MutableWrapper<>(Boolean.FALSE);
        final File jobInputFile = m_clientManager.getJobInputFile(jobProperties, deleteInputFile);
        if (jobInputFile == null) {
            return null;
        }

        final String cacheHash = new FileHashBuilder(jobInputFile).getHash(jobType, jobProperties, m_serviceLookup.getOptionalService(CapabilityService.class), session);
        if (cacheHash == null) {
            logError("DC client convert, conversion without casheHash not possible.");
            return null;
        }

        final boolean isAsync = ((Boolean)jobProperties.getOrDefault(Properties.PROP_ASYNC, Boolean.FALSE)).booleanValue();
        final boolean noCache = ((Boolean)jobProperties.getOrDefault(Properties.PROP_NO_CACHE, Boolean.FALSE)).booleanValue();

        jobProperties.remove(Properties.PROP_INPUT_STREAM);
        jobProperties.put(Properties.PROP_INPUT_FILE, jobInputFile);
        jobProperties.put(Properties.PROP_CACHE_HASH, cacheHash);

        final boolean trace = DocumentConverterManager.isLogTrace();
        final String infoFileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        long traceStartTimeMillis = 0;
        if (trace) {
            traceStartTimeMillis = System.currentTimeMillis();
            DocumentConverterManager.logTrace("DC client received request for " + (isAsync ? "async " : "") + "conversion", jobProperties);
        }

        InputStream ret = null;
        if (isAsync) {
            if (ManagedClientConverterJob.CheckJobInputFileSize(this, jobProperties, resultProperties) &&
                    ManagedClientConverterJob.CheckSupportedJobInputType(this, jobProperties, resultProperties)) {

                boolean triggerExecution = false;
                if (noCache) {
                    triggerExecution = true;
                }
                else {
                    final boolean isCached = getCacheServiceClient() != null && getCacheServiceClient().hasCacheEntry(cacheHash, infoFileName);
                    triggerExecution = !isCached;
                }
                if (triggerExecution) {
                    m_asyncExecutor.triggerExecution(jobType, cacheHash, jobProperties, resultProperties);
                }
            }
        }
        else {
            if (getCacheServiceClient() != null) {
                ret = getCacheServiceClient().getCacheResultStream(cacheHash, infoFileName);
            }
            if (ret == null) {
                ret = ManagedClientConverterJob.process(jobType, jobProperties, resultProperties);

                if ((ret != null && trace) || (ret == null && DocumentConverterManager.isLogWarn())) {
                    final String log = DocumentConverterManager.implGetLogMessage("DC client convert " + (null != ret ? "succeeded" : "failed"), null,
                            new LogData("cacheehash", cacheHash),
                            new LogData("filename", infoFileName),
                            new LogData("exectime", Long.toString(System.currentTimeMillis() - traceStartTimeMillis) + "ms"));

                    if (ret != null) {
                        DocumentConverterManager.logTrace(log);
                    }
                    else {
                        DocumentConverterManager.logWarn(log);
                    }
                }
            }
        }
        if (deleteInputFile.get().booleanValue()) {
            FileUtils.deleteQuietly(jobInputFile);
        }
        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    public int getRemoteAPIVersionDC(final boolean force) {
        return (null != m_remoteValidator_DC) ? m_remoteValidator_DC.getRemoteAPIVersion(force) : 0;
    }

    public int getRemoteAPIVersionCS(final boolean force) {
        return (null != m_remoteValidator_CS) ? m_remoteValidator_CS.getRemoteAPIVersion(force) : 0;
    }

    /**
     * Resetting the current remote status.
     * This method should be called, whenever
     * there is an error with the current connection,
     * so that we know, that there's no current
     * connection available.
     */
    public void resetRemoteConnectionDC() {
        if ((null != m_remoteValidator_DC) && m_remoteValidator_DC.isRemoteServerSupported()) {
            // reset the status flag at validator
            m_remoteValidator_DC.resetConnection();
        }
    }

    public void resetRemoteConnectionCS() {
        if ((null != m_remoteValidator_CS) && m_remoteValidator_CS.isRemoteServerSupported()) {
            // reset the status flag at validator
            m_remoteValidator_CS.resetConnection();
        }
    }

    // - Config ----------------------------------------------------------------

    /**
     * @return
     */
    public DocumentConverterClientMetrics getDocumentConverterClientMetrics() {
        return m_clientMetrics;
    }

    /**
     * @return
     */
    public ClientConfig getClientConfig() {
        return m_clientConfig;
    }

    // - PDFExtractor ----------------------------------------------------------

    @Nullable public PDFExtractor getPDFExtractor() {
        return m_pdfExtractor;
    }

    // - CacheServiceClient  ---------------------------------------------------

    public @Nullable CacheServiceClient getCacheServiceClient() {
        return m_cacheServiceClient;
    }

    // - Statics ---------------------------------------------------------------

    final private static int ASYNC_CONVERTER_THREADCOUNT = 3;

    private static ClientManager m_clientManager = null;

    // - ClientManager ---------------------------------------------------------

    final private DocumentConverterClientMetrics m_clientMetrics = new DocumentConverterClientMetrics();

    final private ClientConfig m_clientConfig;

    final private ServiceLookup m_serviceLookup;

    final private RemoteValidator m_remoteValidator_DC;

    final private RemoteValidator m_remoteValidator_CS;

    final private AsyncExecutor m_asyncExecutor;

    private PDFExtractor m_pdfExtractor = null;

    private CacheServiceClient m_cacheServiceClient = null;
}
