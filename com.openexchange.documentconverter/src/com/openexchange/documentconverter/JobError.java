/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.util.Arrays;
import java.util.function.Consumer;

//-------------
//- JobStatus -
//-------------

/**
 * {@link JobError}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public enum JobError {

    NONE(JobError.ERRORCODE_NONE, "none", "ERROR_NONE"),
    GENERAL(JobError.ERRORCODE_GENERAL, "filterError", "ERROR_GENERAL_ERROR"),
    TIMEOUT(JobError.ERRORCODE_TIMEOUT, "timeout", "ERROR_TIMEOUT"),
    PASSWORD(JobError.ERRORCODE_PASSWORD, "passwordProtected", "ERROR_PASSWORD_PROTECTED"),
    NO_CONTENT(JobError.ERRORCODE_NO_CONTENT, "noContent","ERROR_NO_CONTENT"),
    MAX_SOURCESIZE(JobError.ERRORCODE_MAX_SOURCESIZE, "maxSourceSizeExceeded", "ERROR_MAX_SOURCE_SIZE_EXCEEDED"),
    DISPOSED(JobError.ERRORCODE_DISPOSED, "engineDisposed", "ERROR_CONVERSION_ENGINE_DISPOSED"),
    UNSUPPORTED(JobError.ERRORCODE_UNSUPPORTED, "unsupportedType", "ERROR_UNSUPPORTED_INPUT_TYPE"),
    OUT_OF_MEMORY(JobError.ERRORCODE_OUT_OF_MEMORY, "outOfMemory", "ERROR_OUT_OF_MEMORY"),
    ABORT(JobError.ERRORCODE_ABORT, "abort", "ERROR_ABORT"),
    PDFTOOL(JobError.ERRORCODE_PDFTOOL, "pdfTool", "ERROR_PDFTOOL"),
    MAX_QUEUE_COUNT(JobError.ERRORCODE_MAX_QUEUE_COUNT, "maxQueueCount", "ERROR_MAX_QUEUE_COUNT"),
    QUEUE_TIMEOUT(JobError.ERRORCODE_QUEUE_TIMEOUT, "queueTimeout", "ERROR_QUEUE_TIMEOUT"),
    SERVER_CONNECTION_ERROR(JobError.ERRORCODE_SERVER_CONNECTION_ERROR, "serverConnection", "ERROR_SERVER_CONNECTION_ERROR"),
    SOURCE_NOT_ACCESSIBLE(JobError.ERRORCODE_SOURCE_NOT_ACCESSIBLE, "sourceNotAccessible", "ERROR_SOURCE_NOT_ACCESSIBLE");

    // -------------------------------------------------------------------------

    /**
     * Initializes a new {@link JobError}.
     * @param errorCode
     * @param errorText
     */
    JobError(int errorCode, String errorText, String errorCodeRepresentation) {
        m_errorCode = errorCode;
        m_errorText = errorText;
        m_errorCodeRepresentation = errorCodeRepresentation;
    }

    /**
     * @return The error code id of the enum
     */
    final public int getErrorCode() {
        return m_errorCode;
    }

    /**
     * @return The textual short name of the current error
     */
    final public String getErrorText() {
        return m_errorText;
    }

    /**
     * @return The textual representation of the current error
     */
    final public String getErrorCodeRepresentation() {
        return m_errorCodeRepresentation;
    }

    /**
     * @param errorCode
     * @return The enum with the error code id
     */
    public static JobError fromErrorCode(int errorCode) {
        JobError ret = NONE;

        switch (errorCode) {
            case ERRORCODE_NONE:
                ret = NONE;
                break;
            case ERRORCODE_GENERAL:
                ret = GENERAL;
                break;
            case ERRORCODE_TIMEOUT:
                ret = TIMEOUT;
                break;
            case ERRORCODE_PASSWORD:
                ret = PASSWORD;
                break;
            case ERRORCODE_NO_CONTENT:
                ret = NO_CONTENT;
                break;
            case ERRORCODE_MAX_SOURCESIZE:
                ret = MAX_SOURCESIZE;
                break;

            case ERRORCODE_DISPOSED:
                ret = DISPOSED;
                break;

            case ERRORCODE_UNSUPPORTED:
                ret = UNSUPPORTED;
                break;

            case ERRORCODE_OUT_OF_MEMORY:
                ret = JobError.OUT_OF_MEMORY;
                break;

            case ERRORCODE_ABORT:
                ret = JobError.ABORT;
                break;

            case ERRORCODE_PDFTOOL:
                ret = JobError.PDFTOOL;
                break;

            case ERRORCODE_MAX_QUEUE_COUNT:
                ret = JobError.MAX_QUEUE_COUNT;
                break;

            case ERRORCODE_QUEUE_TIMEOUT:
                ret = JobError.QUEUE_TIMEOUT;
                break;

            case ERRORCODE_SERVER_CONNECTION_ERROR:
                ret = JobError.SERVER_CONNECTION_ERROR;
                break;

            case ERRORCODE_SOURCE_NOT_ACCESSIBLE:
                ret = JobError.SOURCE_NOT_ACCESSIBLE;
                break;

            default:
                ret = GENERAL;
                break;
        }

        return ret;
    }

    /**
     * @param action
     */
    public static void forEach(Consumer<? super JobError> action) {
        Arrays.stream(values()).forEach(action);
    }

    // - Members ---------------------------------------------------------------

    private int m_errorCode = ERRORCODE_NONE;

    private String m_errorText = "none";

    private String m_errorCodeRepresentation = "ERROR_NONE";

    // - Constants -------------------------------------------------------------

    // the error codes to be mapped to enums and vice versa
    final private static int ERRORCODE_NONE                     = 0x00000000;
    final private static int ERRORCODE_GENERAL                  = 0x00000001;
    final private static int ERRORCODE_TIMEOUT                  = 0x00000002;
    final private static int ERRORCODE_PASSWORD                 = 0x00000004;
    final private static int ERRORCODE_NO_CONTENT               = 0x00000008;
    final private static int ERRORCODE_MAX_SOURCESIZE           = 0x00000010;
    final private static int ERRORCODE_DISPOSED                 = 0x00000020;
    final private static int ERRORCODE_UNSUPPORTED              = 0x00000040;
    final private static int ERRORCODE_OUT_OF_MEMORY            = 0x00000080;
    final private static int ERRORCODE_ABORT                    = 0x00000100;
    final private static int ERRORCODE_PDFTOOL                  = 0x00000200;
    final private static int ERRORCODE_MAX_QUEUE_COUNT          = 0x00000400;
    final private static int ERRORCODE_QUEUE_TIMEOUT            = 0x00000800;
    final private static int ERRORCODE_SERVER_CONNECTION_ERROR  = 0x00001000;
    final private static int ERRORCODE_SOURCE_NOT_ACCESSIBLE    = 0x00002000;
}
