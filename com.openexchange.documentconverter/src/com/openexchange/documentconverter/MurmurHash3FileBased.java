/*
 * This is a modified version of the MurmurHash3 algorithm taken
 * from org.apache.commons.codec.digest.MurmurHash3.
 * 
 * This version contains the 128 bit variant only using a File input
 * interface.
 * 
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openexchange.documentconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Implementation of the MurmurHash3 32-bit and 128-bit hash functions.
 *
 * <p>
 * MurmurHash is a non-cryptographic hash function suitable for general hash-based lookup. The name comes from two basic
 * operations, multiply (MU) and rotate (R), used in its inner loop. Unlike cryptographic hash functions, it is not
 * specifically designed to be difficult to reverse by an adversary, making it unsuitable for cryptographic purposes.
 * </p>
 *
 * <p>
 * This contains a Java port of the 32-bit hash function {@code MurmurHash3_x86_32} and the 128-bit hash function
 * {@code MurmurHash3_x64_128} from Austin Appleby's original {@code c++} code in SMHasher.
 * </p>
 *
 * <p>
 * This is public domain code with no copyrights. From home page of
 * <a href="https://github.com/aappleby/smhasher">SMHasher</a>:
 * </p>
 *
 * <blockquote> "All MurmurHash versions are public domain software, and the author disclaims all copyright to their
 * code." </blockquote>
 *
 * <p>
 * Original adaption from Apache Hive. That adaption contains a {@code hash64} method that is not part of the original
 * MurmurHash3 code. It is not recommended to use these methods. They will be removed in a future release. To obtain a
 * 64-bit hash use half of the bits from the {@code hash128x64} methods using the input data converted to bytes.
 * </p>
 *
 * @see <a href="https://en.wikipedia.org/wiki/MurmurHash">MurmurHash</a>
 * @see <a href="https://github.com/aappleby/smhasher/blob/master/src/MurmurHash3.cpp"> Original MurmurHash3 c++
 *      code</a>
 * @see <a href=
 *      "https://github.com/apache/hive/blob/master/storage-api/src/java/org/apache/hive/common/util/Murmur3.java">
 *      Apache Hive Murmer3</a>
 * @since 1.13
 */
public final class MurmurHash3FileBased {

    /**
     * A random number to use for a hash code.
     *
     * @deprecated This is not used internally and will be removed in a future release.
     */
    @Deprecated
    public static final long NULL_HASHCODE = 2862933555777941757L;
    /**
     * A default seed to use for the murmur hash algorithm.
     * Has the value {@code 104729}.
     */
    public static final int DEFAULT_SEED = 104729;

    // Constants for 128-bit variant
    private static final long C1 = 0x87c37b91114253d5L;
    private static final long C2 = 0x4cf5ad432745937fL;
    private static final int R1 = 31;
    private static final int R2 = 27;
    private static final int R3 = 33;
    private static final int M = 5;

    private static final int N1 = 0x52dce729;

    private static final int N2 = 0x38495ab5;

    /**
     * Performs the final avalanche mix step of the 64-bit hash function {@code MurmurHash3_x64_128}.
     *
     * @param hash The current hash
     * @return The final hash
     */
    private static long fmix64(long hash) {
        hash ^= hash >>> 33;
        hash *= 0xff51afd7ed558ccdL;
        hash ^= hash >>> 33;
        hash *= 0xc4ceb9fe1a85ec53L;
        hash ^= hash >>> 33;
        return hash;
    }

    /**
     * Gets the little-endian long from 8 bytes starting at the specified index.
     *
     * @param data The data
     * @param index The index
     * @return The little-endian long
     */
    private static long getLittleEndianLong(final byte[] data, final int index) {
        return (long) data[index    ] & 0xff |
               ((long) data[index + 1] & 0xff) <<  8 |
               ((long) data[index + 2] & 0xff) << 16 |
               ((long) data[index + 3] & 0xff) << 24 |
               ((long) data[index + 4] & 0xff) << 32 |
               ((long) data[index + 5] & 0xff) << 40 |
               ((long) data[index + 6] & 0xff) << 48 |
               ((long) data[index + 7] & 0xff) << 56;
    }

    /**
     * Generates 128-bit hash from the byte array with a default seed.
     * This is a helper method that will produce the same result as:
     *
     * <pre>
     * int offset = 0;
     * int seed = 104729;
     * int hash = MurmurHash3.hash128(data, offset, data.length, seed);
     * </pre>
     *
     * <p>Note: The sign extension bug in {@link #hash128(byte[], int, int, int)} does not effect
     * this result as the default seed is positive.</p>
     *
     * @param data The input byte array
     * @return The 128-bit hash (2 longs)
     * @see #hash128(byte[], int, int, int)
     */
    public static long[] hash128(final byte[] data) {
        return hash128x64(data, 0, data.length, DEFAULT_SEED);
    }

    /**
     * Generates 128-bit hash from the byte array with the given offset, length and seed.
     *
     * <p>This is an implementation of the 128-bit hash function {@code MurmurHash3_x64_128}
     * from Austin Appleby's original MurmurHash3 {@code c++} code in SMHasher.</p>
     *
     * @param data The input byte array
     * @param offset The first element of array
     * @param length The length of array
     * @param seed The initial seed value
     * @return The 128-bit hash (2 longs)
     * @since 1.14
     */
    public static long[] hash128x64(final byte[] data, final int offset, final int length, final int _seed) {
        return finalize(hash128x64Internal(data, offset, length, new long[] { _seed & 0xffffffffL, _seed & 0xffffffffL }), length);
        
    }

    public static long[] hash128x64(File file) throws IOException {
        long[] hash = { DEFAULT_SEED, DEFAULT_SEED };
        int totalSize = 0;
        try(FileInputStream inputStream = new FileInputStream(file)) {
            final byte[] buffer = new byte[8192];
            int bytesRead;
            while((bytesRead = inputStream.read(buffer)) != -1) {
                hash = hash128x64Internal(buffer, 0, bytesRead, hash);
                totalSize += bytesRead;
            }
        }
        if (totalSize == 0) {
            throw new IOException("MurmurHash3FileBased, file size is 0");
        }
        return finalize(hash, totalSize);
    }

    /**
     * Generates 128-bit hash from the byte array with the given offset, length and seed.
     *
     * <p>This is an implementation of the 128-bit hash function {@code MurmurHash3_x64_128}
     * from Austin Appleby's original MurmurHash3 {@code c++} code in SMHasher.</p>
     *
     * @param data The input byte array
     * @param offset The first element of array
     * @param length The length of array
     * @param seed The initial seed value
     * @return The 128-bit hash (2 longs)
     */
    @SuppressWarnings("fallthrough")
    private static long[] hash128x64Internal(final byte[] data, final int offset, final int length, long[] seed) {

        long h1 = seed[0];
        long h2 = seed[1];
        final int nblocks = length >> 4;

        // body
        for (int i = 0; i < nblocks; i++) {
            final int index = offset + (i << 4);
            long k1 = getLittleEndianLong(data, index);
            long k2 = getLittleEndianLong(data, index + 8);

            // mix functions for k1
            k1 *= C1;
            k1 = Long.rotateLeft(k1, R1);
            k1 *= C2;
            h1 ^= k1;
            h1 = Long.rotateLeft(h1, R2);
            h1 += h2;
            h1 = h1 * M + N1;

            // mix functions for k2
            k2 *= C2;
            k2 = Long.rotateLeft(k2, R3);
            k2 *= C1;
            h2 ^= k2;
            h2 = Long.rotateLeft(h2, R1);
            h2 += h1;
            h2 = h2 * M + N2;
        }

        // tail
        long k1 = 0;
        long k2 = 0;
        final int index = offset + (nblocks << 4);
        switch (offset + length - index) {
        case 15:
            k2 ^= ((long) data[index + 14] & 0xff) << 48;
        case 14:
            k2 ^= ((long) data[index + 13] & 0xff) << 40;
        case 13:
            k2 ^= ((long) data[index + 12] & 0xff) << 32;
        case 12:
            k2 ^= ((long) data[index + 11] & 0xff) << 24;
        case 11:
            k2 ^= ((long) data[index + 10] & 0xff) << 16;
        case 10:
            k2 ^= ((long) data[index + 9] & 0xff) << 8;
        case 9:
            k2 ^= data[index + 8] & 0xff;
            k2 *= C2;
            k2 = Long.rotateLeft(k2, R3);
            k2 *= C1;
            h2 ^= k2;

        case 8:
            k1 ^= ((long) data[index + 7] & 0xff) << 56;
        case 7:
            k1 ^= ((long) data[index + 6] & 0xff) << 48;
        case 6:
            k1 ^= ((long) data[index + 5] & 0xff) << 40;
        case 5:
            k1 ^= ((long) data[index + 4] & 0xff) << 32;
        case 4:
            k1 ^= ((long) data[index + 3] & 0xff) << 24;
        case 3:
            k1 ^= ((long) data[index + 2] & 0xff) << 16;
        case 2:
            k1 ^= ((long) data[index + 1] & 0xff) << 8;
        case 1:
            k1 ^= data[index] & 0xff;
            k1 *= C1;
            k1 = Long.rotateLeft(k1, R1);
            k1 *= C2;
            h1 ^= k1;
        }
        return new long[] { h1, h2 };
    }

    private static long[] finalize(long[] hash, final long length) {
        long h1 = hash[0];
        long h2 = hash[1];

        // finalization
        h1 ^= length;
        h2 ^= length;

        h1 += h2;
        h2 += h1;

        h1 = fmix64(h1);
        h2 = fmix64(h2);

        h1 += h2;
        h2 += h1;

        return new long[] { h1, h2 };
    }
}
