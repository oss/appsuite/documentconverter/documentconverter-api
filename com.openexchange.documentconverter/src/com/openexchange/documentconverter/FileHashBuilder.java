/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.exception.OXException;
import com.openexchange.session.Session;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class FileHashBuilder {
    /**
     * @param file The source file
     * @param locale The locale to be used, might be 'null'
     * @return
     * @throws IOException 
    */
    public FileHashBuilder(File file) {
        String _fileHash = null;
        try {
            final long[] hash128 = MurmurHash3FileBased.hash128x64(file);
            final StringBuilder builder = new StringBuilder()
                .append(Long.toHexString(hash128[0]))
                .append(Long.toHexString(hash128[1]));
            while(builder.length() < 32) {
                builder.append('0');
            }
            _fileHash = builder.toString();
        } catch (IOException e) {
            //
        }
        fileHash = _fileHash;
    }

    private final String fileHash;

    // ------------------------------------------------------------------------------------------------------

    public @Nullable String getHash(@NonNull String jobType, @NonNull HashMap<String, Object> jobProperties, CapabilityService capabilityService, Session session) {
        if (fileHash == null) {
            DocumentConverterManager.logError("DC Client FileHashBuilder, fileHash is null, jobType: " + jobType);
            return null;
        }

        final StringBuilder builder = new StringBuilder(fileHash).append('-').append(jobType);
        final String locale = (String)jobProperties.get(Properties.PROP_LOCALE);

        switch(jobType) {
            case "ooxml" : // !!PASSTHROUGH_INTENDED
            case "odf" :   // !!PASSTHROUGH_INTENDED
            case "pdf" : {
                break;
            }
            case "preview" :
            // passthrough intended. Own jobType only for the reason to differentiate between previews of the
            // DocumentConverterPreviewService and normal "graphic" conversion jobs e.g. wmf to jpg
            case "graphic" : {
                final String mimeType = ((String)jobProperties.getOrDefault(Properties.PROP_MIME_TYPE, "")).toLowerCase();
                if (mimeType.isBlank()) {
                    DocumentConverterManager.logError("DC Client FileHashBuilder, mimeType is needed for jobType graphic");
                    return null;
                }
                builder.append('-').append("image/jpeg".equals(mimeType) ? "jpg" : "png")
                    .append('-').append((String)jobProperties.getOrDefault(Properties.PROP_IMAGE_SCALE_TYPE, "cover"))
                    .append('@').append(Integer.valueOf(((Integer)jobProperties.getOrDefault(Properties.PROP_PIXEL_WIDTH, Integer.valueOf(-1))).intValue()))
                    .append('x').append(((Integer)jobProperties.getOrDefault(Properties.PROP_PIXEL_HEIGHT, Integer.valueOf(-1))).intValue());
                break;
            }
            case "shape2png": {
                final boolean noCache = ((Boolean)jobProperties.getOrDefault(Properties.PROP_NO_CACHE, Boolean.FALSE)).booleanValue();
                if (!noCache) {
                    // the cacheHash is having this format: 1289d8187cd00f0aff1a7c9818523aa-shape2png, its a hash of the shape and not of the file
                    return (String)jobProperties.get(Properties.PROP_CACHE_HASH);
                }

                // if noCashe is set, the hashes have to be provided via shapeReplacements property
                if (jobProperties.get(Properties.PROP_SHAPE_REPLACEMENTS) == null) {
                    return null;
                }
                // for async trigger we use the file hash like this: 5d1a8ce484bd7443f2fc5de1adeb8269-replacements-shape2png
                builder.append("-replacements"); // the file hash is needed to prevent async requests to trigger multiple times (AsyncExecutor)  
                break;
            }
            default: {
                DocumentConverterManager.logError("DC Client FileHashBuilder, unsupported jobType used: " + jobType);
                return null;
            }
        }

        // capabilities
        final String fileName = (String)jobProperties.get(Properties.PROP_INFO_FILENAME);
        if (("pdf").equals(jobType) && "xml".equals(FilenameUtils.getExtension(fileName).toLowerCase())) {
            if (capabilityService != null && session != null) {
                try {
                    final CapabilitySet capabilitySet = capabilityService.getCapabilities(session);
                    if (capabilitySet.contains("document_preview_xrechnung")) {
                        builder.append("-cap.xr");
                    }
                }
                catch(OXException e) {
                    DocumentConverterManager.logError("DC Client FileHashBuilder, Error getting capabilities for user (id):" + Integer.valueOf(session.getUserId()).toString());
                }
            }
        }

        // locale
        if (StringUtils.isNotEmpty(locale) && !locale.startsWith("en")) {   // 'en*' is considered to be the default locale; add all other locales to the
            builder.append("-lang.").append(locale);                        // input file hash in order to be able to perform locale based conversions
        }

        // page range
        final String pageRange = (String)jobProperties.get(Properties.PROP_PAGE_RANGE);
        if (StringUtils.isNotEmpty(pageRange)) {
            builder.append('#').append(pageRange);
        }
        return builder.toString();
    }
}
