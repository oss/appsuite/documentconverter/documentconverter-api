/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.metrics.micrometer.Micrometer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link DocumentConverterMetrics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class DocumentConverterClientMetrics {

    final public static String CONNECTION_STATUS_OK = "ok";
    final public static String CONNECTION_STATUS_UNKNOWNERROR = "unknownerror";

    /**
     * {@link ConnectionStatus}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.16.0
     */
    public enum ConnectionType {
        VALIDATION_DC(".validation.count.dc", "10f3d443-c8b9-4149-a15c-415a6fc63fba"),
        VALIDATION_CS(".validation.count.cs", "7ec74ed2-c928-11eb-bd36-6fd14434831d"),
        CONVERT(".convert.count", null);

        ConnectionType(String metricSuffix, @Nullable String serverId) {
            this.metricSuffix = metricSuffix;
            this.serverId = serverId;
        }

        public String getMetricSuffix() {
            return metricSuffix;
        }

        public @Nullable String getServerId() {
            return serverId;
        }

        private final String metricSuffix;
        private final String serverId;
    }

    private static final String GROUP = "appsuite.documentconverterclient";
    private static final String CONNECTIONS = ".connections";
    private static final String NO_UNIT = null;

    private static final Tags TAGS_CONNECTION_OK = Tags.of("status", "ok");

    // -------------------------------------------------------------------------

    /**
     * Initializes a new {@link DocumentConverterClientMetrics}.
     */
    public DocumentConverterClientMetrics() {
        super();
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void incrementConnectionStatusCount(@NonNull final ConnectionType connectionType, @Nullable final Exception e) {
        String connectionStatus = CONNECTION_STATUS_OK;

        if (null != e) {
            final String className = e.getClass().getName();
            final int lastDotPos = className.lastIndexOf(".");

            connectionStatus = (-1 == lastDotPos) ? className : className.substring(lastDotPos + 1);
        }

        Map<Tags, AtomicLong> _countMapToUse = countMaps.get(connectionType);
        if (_countMapToUse == null) {
            _countMapToUse = new HashMap<Tags, AtomicLong>();
            countMaps.put(connectionType, _countMapToUse);
        }

        final Map<Tags, AtomicLong> countMapToUse = _countMapToUse;
        final var curTags = CONNECTION_STATUS_OK.equals(connectionStatus) ? TAGS_CONNECTION_OK : Tags.of("status", connectionStatus);

        synchronized (countMapToUse) {
            AtomicLong curCount = countMapToUse.get(curTags);

            if (null == curCount) {
                countMapToUse.put(curTags, curCount = new AtomicLong(0));

                Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + CONNECTIONS + connectionType.getMetricSuffix(), curTags,
                    "The number of established or lost connections to DocumentConverter server.", NO_UNIT, this,
                    (m) -> countMapToUse.get(curTags).get());
            }

            curCount.incrementAndGet();
        }
    }

    // - Members ----------------------------------------------------------------

    final private Map<ConnectionType, Map<Tags, AtomicLong>> countMaps = new HashMap<>();
}
