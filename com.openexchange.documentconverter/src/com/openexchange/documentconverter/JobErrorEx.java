/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.documentconverter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@link JobErrorEx}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class JobErrorEx {

    public final static String JSON_KEY_ORIGIN = "origin";

    public final static String JSON_KEY_ERROR_CODE = "errorCode";

    public final static String JSON_KEY_STATUS_ERROR_CACHE = "statusErrorCache";

    public final static String JSON_KEY_DESCRIPTION = "description";

    public final static String JSON_KEY_DESCRIPTION_DATA = "descriptionData";

    public final static String JSON_VALUE_DESCRIPTION_GENERIC_NONE = "None";

    public final static String JSON_VALUE_DESCRIPTION_GENERIC_ERROR = "Generic error";

    public final static String JSON_VALUE_DESCRIPTION_DATA_EMPTY = JSONObject.EMPTY_OBJECT.toString();


    /**
     * {@link StatusErrorCache}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum StatusErrorCache {
        NONE(StatusErrorCache.STATUS_NONE),
        DISABLED(StatusErrorCache.STATUS_DISABLED),
        NEW(StatusErrorCache.STATUS_NEW),
        ACTIVE(StatusErrorCache.STATUS_ACTIVE),
        LOCKED(StatusErrorCache.STATUS_LOCKED);

        /**
         * Initializes a new {@link StatusErrorCache}.
         * @param statusText
         */
        StatusErrorCache(String statusText) {
            m_statusText = statusText;
        }

        /**
         * @return
         */
        public String getStatusText() {
            return m_statusText;
        }

        /**
         * @param statusText
         * @return
         */
        public static StatusErrorCache fromStatusText(String statusText) {
            if (STATUS_NONE.equalsIgnoreCase(statusText)) {
                return NONE;
            } else if (STATUS_DISABLED.equalsIgnoreCase(statusText)) {
                return DISABLED;
            } else if (STATUS_NEW.equalsIgnoreCase(statusText)) {
                return NEW;
            } else if (STATUS_ACTIVE.equalsIgnoreCase(statusText)) {
                return ACTIVE;
            } else if (STATUS_LOCKED.equalsIgnoreCase(statusText)) {
                return LOCKED;
            }

            return NONE;
        }

        // - Members -----------------------------------------------------------

        final String m_statusText;

        // - static members ----------------------------------------------------

        final private static String STATUS_NONE = "none";
        final private static String STATUS_DISABLED = "disabled";
        final private static String STATUS_NEW = "new";
        final private static String STATUS_ACTIVE = "active";
        final private static String STATUS_LOCKED = "locked";
    }

    /**
     * Initializes a new {@link JobErrorEx}.
     */
    public JobErrorEx() {
        super();

        m_jobError = JobError.NONE;
    }

    /**
     * Initializes a new {@link JobErrorEx}.
     * @param jobError
     */
    public JobErrorEx(final JobError jobError) {
        super();

        m_jobError = (null != jobError) ? jobError : JobError.NONE;
    }

    /**
     * @return
     */
    public boolean hasError() {
        return (JobError.NONE != m_jobError);
    }

    /**
     * @return
     */
    public boolean hasNoError() {
        return (JobError.NONE == m_jobError);
    }

    /**
     * @return The {@link JobError} of this instance
     */
    public JobError getJobError() {
        return m_jobError;
    }

    /**
     * @return The error code id of the enum
     */
    public int getErrorCode() {
        return m_jobError.getErrorCode();
    }

    /**
     * @return The textual representation of the current error
     */
    public String getErrorText() {
        return m_jobError.getErrorText();
    }

    /**
     * @param statusErrorCache
     */
    public synchronized void setStatusErrorCache(final StatusErrorCache statusErrorCache) {
        if (null != statusErrorCache) {
            m_statusErrorCache = statusErrorCache;
        }
    }

    /**
     * @return
     */
    public synchronized StatusErrorCache getStatusErrorCache() {
        return m_statusErrorCache;
    }

    /**
     * @param errorDescription
     */
    public synchronized void setErrorData(final String errorDescription) {
        m_errorDescription = errorDescription;
        m_errorData = null;
    }

    /**
     * @param errorDescription
     * @param errorData
     */
    public synchronized void setErrorData(final String errorDescription, final JSONObject errorData) {
        m_errorDescription = errorDescription;
        m_errorData = errorData;
    }

    /**
     * @return The extended JSON description of this error
     */
    public synchronized JSONObject getErrorData() {
        final JSONObject jsonErrorData = new JSONObject();

        try {
            jsonErrorData.put(JSON_KEY_ERROR_CODE, m_jobError.getErrorCodeRepresentation());

            if (StatusErrorCache.NONE != m_statusErrorCache) {
                jsonErrorData.put(JSON_KEY_STATUS_ERROR_CACHE, m_statusErrorCache.getStatusText());
            }

            if (null != m_errorDescription) {
                jsonErrorData.put(JSON_KEY_DESCRIPTION, m_errorDescription);
            }

            if (null != m_errorData) {
                jsonErrorData.put(JSON_KEY_DESCRIPTION_DATA, m_errorData);
            }
        } catch (JSONException e) {
            DocumentConverterManager.logExcp(e);
        }

        return jsonErrorData;
    }

    /**
     * @param resultProperties
     * @return
     */
    public static JobErrorEx fromResultProperties(final Map<String, Object> resultProperties) {
        JobErrorEx jobErrorEx = new JobErrorEx();

        if (null != resultProperties) {
            final Object resultCodeNumberObj = resultProperties.get(Properties.PROP_RESULT_ERROR_CODE);

            if (resultCodeNumberObj instanceof Number) {
                jobErrorEx = new JobErrorEx(JobError.fromErrorCode(((Number) resultCodeNumberObj).intValue()));

                if (jobErrorEx.hasError()) {
                    final String errorData = (String) resultProperties.get(Properties.PROP_RESULT_ERROR_DATA);

                    if (StringUtils.isNotBlank(errorData)) {
                        try {
                            final JSONObject jsonErrorData = new JSONObject(errorData);

                            if (jsonErrorData.has(JSON_KEY_STATUS_ERROR_CACHE)) {
                                jobErrorEx.setStatusErrorCache(StatusErrorCache.fromStatusText(jsonErrorData.getString(JSON_KEY_STATUS_ERROR_CACHE)));
                            }

                            if (jsonErrorData.has(JSON_KEY_DESCRIPTION)) {
                                jobErrorEx.setErrorData(
                                    jsonErrorData.getString(JSON_KEY_DESCRIPTION),
                                    jsonErrorData.optJSONObject(JSON_KEY_DESCRIPTION_DATA));
                            }

                        } catch (JSONException e) {
                            DocumentConverterManager.logExcp(e);
                            jobErrorEx = new JobErrorEx(JobError.GENERAL);
                        }
                    }
                }
            }
        }

        return jobErrorEx;
    }

    /**
     * @param action
     */
    public static void forEach(Consumer<? super JobError> action) {
        Arrays.stream(JobError.values()).forEach(action);
    }

    // - Members ---------------------------------------------------------------

    final private JobError m_jobError;

    private String m_errorDescription = null;

    private JSONObject m_errorData = null;

    private StatusErrorCache m_statusErrorCache = DEFAULT_STATUS_ERROR_CACHE;

    // - static Members --------------------------------------------------------

    private static StatusErrorCache DEFAULT_STATUS_ERROR_CACHE = StatusErrorCache.NONE;

    /**
     *
     */
    public synchronized static void setErrorCacheDisabled() {
        DEFAULT_STATUS_ERROR_CACHE = StatusErrorCache.DISABLED;
    }
}
