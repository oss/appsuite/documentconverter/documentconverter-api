/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.exception.Category;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;

/**
 * {@link DocumentConverterManager}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
public class DocumentConverterManager {

    final public static int HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS=60000;

    final public static int HTTP_DOCUMENTCONVERTER_READ_TIMEOUT_MS=(int) ((HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS * 210L) / 100L);

    final public static int HTTP_CACHESERVER_CONNECT_TIMEOUT_MS=5000;

    final public static int HTTP_CACHESERVER_READ_TIMEOUT_MS=60000;

    final public static String[] BASH_SEARCH_PATHES = {
        "/bin/bash",
        "/sbin/bash",
        "/usr/bin/bash",
        "/usr/local/bin/bash",
        "/usr/local/sbin/bash"
    };

    /**
     * Initializes a new {@link DocumentConverterManager}.
     */
    public DocumentConverterManager(@NonNull final File tmpRootDir) {
        super();

        m_tmpRootDir = tmpRootDir;
    }

    /**
     * @param tmpFilePrefix The mandatory tmp file prefix
     * @return The tmp file, that was just created of <code>null</code> ij case of an error
     */
    public File createTempFile(@NonNull String tmpFilePrefix) {
        File ret = null;

        if (validateOrMkdir(m_tmpRootDir)) {
            // create a temp. file with the name 'prefix*.tmp' within the configured documentconverter cache directory

            try {
                ret = File.createTempFile(tmpFilePrefix, ".tmp", m_tmpRootDir);
            } catch (IOException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        return ret;
    }

    /**
     * @param tmpDirName The mandatory first directory to create within the tmp root directory
     * @param tmpDirNameN Optional sub directory names to append to all previews sub directories
     * @return The tmp directory, that was just created of <code>null</code> in case of an error
     */
    public File createTempDir(@NonNull final String tmpDirName, final String... tmpDirNameN) {
        File ret = createTempFile(tmpDirName);

        if (ret.canWrite()) {
            FileUtils.deleteQuietly(ret);

            if (ArrayUtils.isNotEmpty(tmpDirNameN)) {
                for (final String curDirToAdd : tmpDirNameN) {
                    ret = new File(ret, curDirToAdd);
                }
            }

            try {
                FileUtils.forceMkdir(ret);
            } catch (IOException e) {
                DocumentConverterManager.logExcp(e);
            }

            if (!ret.exists()) {
                ret = null;
            }
        }

        return ret;
    }

    /**
     * @param dirFile
     * @return
     */
    public static boolean validateOrMkdir(File directoryFile) {
        if (null != directoryFile) {
            if (!directoryFile.exists()) {
                try {
                    FileUtils.forceMkdir(directoryFile);
                } catch (final IOException e) {
                    logExcp(e);
                }

                FileUtils.waitFor(directoryFile, 3);
            }

            return (directoryFile.isDirectory() && directoryFile.canWrite());
        }

        return false;
    }

    /**
     * @param filename
     * @return
     */
    public byte[] getResourceBuffer(String filename) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        byte[] resourceBuffer = null;

        if (null == loader) {
            loader = DocumentConverterManager.class.getClassLoader();
        }

        final URL url = loader.getResource(filename);

        if (null != url) {
            URLConnection connection = null;

            try {
                connection = url.openConnection();

                if (null != connection) {
                    connection.connect();

                    try (InputStream resourceInputStm = connection.getInputStream()) {
                        if (null != resourceInputStm) {
                            resourceBuffer = IOUtils.toByteArray(resourceInputStm);
                        }
                    }
                }
            } catch (final IOException e) {
                logExcp(e);
            }
        }

        return resourceBuffer;
    }

    /**
     * @param jobProperties
     * @param deleteInputFile
     * @return
     */
    public File getJobInputFile(@NonNull HashMap<String, Object> jobProperties, @NonNull MutableWrapper<Boolean> deleteInputFile) {
        File inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

        deleteInputFile.set(Boolean.FALSE);

        // if no input file is given, write content of possible InputStream
        // property to a temp file; the conversion is always done using a real
        // file as input source due to very bad performance with InputStream
        // reads via the UNO bridge (at least with latest LO ReaderEngine)
        if (null == inputFile) {
            try (final InputStream inputStm = (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM)) {
                if (null != inputStm) {
                    if (null != (inputFile = createTempFile("oxcs"))) {
                        try {
                            FileUtils.copyInputStreamToFile(inputStm, inputFile);

                            // set the just filled inputFile as new source at our
                            // job
                            // and remove the previously set input stream;
                            jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
                            jobProperties.remove(Properties.PROP_INPUT_STREAM);

                            // the temporarily created file needs to be deleted by
                            // the caller of this method later in time
                            deleteInputFile.set(Boolean.TRUE);
                        } catch (final IOException e) {
                            logExcp(e);

                            FileUtils.deleteQuietly(inputFile);
                            inputFile = null;
                        }
                    }
                }
            } catch (final IOException e) {
                logExcp(e);
            }
        }

        // check for input type %PDF-x.x, since this input
        // type is explicitly checked at various locations
        if (null != inputFile) {
            try (final InputStream inputStm = FileUtils.openInputStream(inputFile)) {
                final byte[] buffer = new byte[256];

                if ((inputStm.read(buffer) > 0) && new String(buffer, "UTF-8").trim().toLowerCase().startsWith("%pdf-")) {
                    jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
                }
            } catch (final IOException e) {
                logExcp(e);
            }
        }

        return inputFile;
    }

    // static methods
    
    /**
     * @return
     */
    public static int getHTTPConnectTimeoutMillis(ServerType serverType) {
        switch (serverType) {
            case CACHESERVER: {
                return HTTP_CACHESERVER_CONNECT_TIMEOUT_MS;
            }

            case DOCUMENTCONVERTER:
            default: {
                return HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS;
            }
        }
    }

    /**
     * @return
     */
    public static int getHTTPReadTimeoutMillis(ServerType serverType) {
        switch (serverType) {
            case CACHESERVER: {
                return HTTP_CACHESERVER_READ_TIMEOUT_MS;
            }

            case DOCUMENTCONVERTER:
            default: {
                return HTTP_DOCUMENTCONVERTER_READ_TIMEOUT_MS;
            }
        }
    }

    /**
     * @param filename
     */
    public static String getFilenameForLog(String filename) {
        try {
            // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
            // get decoded as this will throw a not declared IllegalArgumentException.
            // In such cases, we replace the illegal hex value with a space char (%25).
            return (null != filename) ?
                URLDecoder.decode(filename.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8") :
                    DocumentConverterUtil.STR_NOT_AVAILABLE;
        } catch (@SuppressWarnings("unused") final Exception e) {
            logTrace("DC client cannot decode given URL, using as is: " + filename);
        }

        return filename;
    }

    /**
     * @param rawBashCommandLine The raw bash line to append commands to the last command element
     * @param commandLine The command parts to append to the last bash command element
     * @return Returns the cloned bash command line array with appended command parts to the last command element
     */
    public static  String[] getCommandLine(final String[] rawBashCommandLine, final String[] commandLine) {
        final String[] cmdLine = (null != rawBashCommandLine) ? rawBashCommandLine.clone() : new String[] { "" };
        final int cmdLineLastElementPos = cmdLine.length - 1;
        final StringBuilder pdfExtractorCmdLine = new StringBuilder(512).
            append(cmdLine[cmdLineLastElementPos]);

        if (null != commandLine) {
            for (final String curCommandLinePart : commandLine) {
                pdfExtractorCmdLine.append(' ').append(curCommandLinePart);
            }
        }

        // replace last command element with updated one
        cmdLine[cmdLineLastElementPos] = pdfExtractorCmdLine.toString();

        return cmdLine;
    }

    /**
     * @param maxVMemMB
     * @return The bash process command array, containing possible
     *  max VMem settings within the last element.
     *  The last element needs to be appended with the
     *  real command to be be finally usable
     */
    public static String[] getMaxVMemBashCommandLine(final long maxVMemMB) {
        String[] ret = null;

        if (maxVMemMB > 0) {
            final String uLimitVMemKbStr = Long.toString(1024L * maxVMemMB);

            for (String curBashPath : BASH_SEARCH_PATHES) {
                if (new File(curBashPath).canExecute()) {
                    ret = new String[] { curBashPath, "-c", new StringBuilder(256).
                        append("ulimit -v ").append(uLimitVMemKbStr).append("; ").
                        append("ulimit -m ").append(uLimitVMemKbStr).append("; ").toString() };

                    break;
                }
            }
        }

        if (null == ret) {
            ret = new String[] { "" };

            final StringBuilder searchPathesBuilder = new StringBuilder(512);

            for (String curBashPath : BASH_SEARCH_PATHES) {
                if (searchPathesBuilder.length() > 0) {
                    searchPathesBuilder.append(':');
                }

                searchPathesBuilder.append(curBashPath);
            }

            logError("DC cannot execute PDFTool due to missing Bash executable. Search path: " + searchPathesBuilder.toString());
        }

        return ret;

    }

    /**
     * @param closeable
     */
    public static void close(final Closeable closeable) {
        if (null != closeable) {
            try {
                closeable.close();
            } catch (IOException e) {
                logExcp(e);
            }
        }
    }

    /**
     * Method impl. is copied from Process#waitFor(long timeout, TimeUnit unit) method
     * with the difference that this method returns the exit code of the process in case
     * of termination in time or -1 in case of timeout or invalid process.
     * This method works on milliseconds only as well.
     * @param process The process to wait for.
     * @param timeoutMillis The milliseconds to wait for the process before -1 is returned.
     * @return The exit code of the process or -1 if process has reached its timeout or process is <code>null</code>.
     */
    public static int waitForProcess(final Process process, final long timeoutMillis) {
        if (null != process) {
            final long startTimeMillis = System.currentTimeMillis();
            long remMillis = timeoutMillis;

            Thread.yield();

            do {
                try {
                    // if process has not yet terminated, an IllegalThreadStateException is thrown
                    final int exitValue = process.exitValue();
                    return exitValue;
                } catch(@SuppressWarnings("unused") IllegalThreadStateException e) {
                    if (remMillis > 0) {
                        try {
                            Thread.sleep(Math.min(remMillis + 1, 50));
                        } catch (@SuppressWarnings("unused") InterruptedException e1) {
                            break;
                        }
                    }
                }

                remMillis = timeoutMillis - (System.currentTimeMillis() - startTimeMillis);
            } while (remMillis > 0);
        }

        return -1;
    }

    // - Logging ---------------------------------------------------------------

    /**
     * @param logProvider
     * @return
     */
    public static boolean isLogTrace() {
        return ((null != LOG) && LOG.isTraceEnabled());
    }

    /**
     * @return true, if the log level 'info' is enabled
     */
    public static boolean isLogDebug() {
        return ((null != LOG) && LOG.isDebugEnabled());
    }

    /**
     * @return true, if the log level 'info' is enabled
     */
    public static boolean isLogInfo() {
        return ((null != LOG) && LOG.isInfoEnabled());
    }

    /**
     * @return true, if the log level 'warn' is enabled
     */
    public static boolean isLogWarn() {
        return ((null != LOG) && LOG.isWarnEnabled());
    }

    /**
     * @return true, if the log level 'error' is enabled
     */
    public static boolean isLogError() {
        return ((null != LOG) && LOG.isErrorEnabled());
    }

    /**
     * @param info
     * @param extraData
     */
    static public void logInfo(final String info, LogData... extraData) {
        logInfo(info, null, extraData);
    }

    /**
     * @param info
     * @param message
     * @param jobProperties
     * @param extraData
     */
    static public void logInfo(final String info, final HashMap<String, Object> jobProperties, LogData... extraData) {
        if (null != LOG) {
            LOG.info(implGetLogMessage(info, jobProperties, extraData));
        }
    }

    /**
     * @param warning
     * @param extraData
     */
    static public void logWarn(final String warning, LogData... extraData) {
        logWarn(warning, null, extraData);
    }

    /**
     * @param warning
     * @param jobProperties
     * @param extraData
     */
    static public void logWarn(final String warning, final HashMap<String, Object> jobProperties, LogData... extraData) {
        if (null != LOG) {
            LOG.warn(implGetLogMessage(warning, jobProperties, extraData));
        }
    }

    /**
     * @param error
     * @param extraData
     */
    static public void logError(final String error, LogData... extraData) {
        logError(error, null, extraData);
    }

    /**
     * @param error
     * @param jobProperties
     * @param extraData
     */
    static public void logError(final String error, final HashMap<String, Object> jobProperties, LogData... extraData) {
        if (null != LOG) {
            LOG.error(implGetLogMessage(error, jobProperties, extraData));
        }
    }

    /**
     * @param error
     * @param extraData
     */
    static public void logDebug(final String debugMsg, LogData... extraData) {
        logDebug(debugMsg, null, extraData);
    }

    /**
     * @param error
     * @param jobProperties
     * @param extraData
     */
    static public void logDebug(final String debugMsg, final HashMap<String, Object> jobProperties, LogData... extraData) {
        if (null != LOG) {
            LOG.debug(implGetLogMessage(debugMsg, jobProperties, extraData));
        }
    }

    /**
     * @param error
     * @param extraData
     */
    static public void logTrace(final String traceMsg, LogData... extraData) {
        logTrace(traceMsg, null, extraData);
    }

    /**
     * @param error
     * @param jobProperties
     * @param extraData
     */
    static public void logTrace(final String traceMsg, final HashMap<String, Object> jobProperties, LogData... extraData) {
        if (null != LOG) {
            LOG.trace(implGetLogMessage(traceMsg, jobProperties, extraData));
        }
    }

    /**
     * @param e
     */
    static public void logExcp(Exception e) {
        final Throwable rootCause = Throwables.getRootCause(e);
        final String rootCauseMessage = (null != rootCause) ? rootCause.getMessage() : DocumentConverterUtil.STR_NOT_AVAILABLE;
        final String lowerCaseRootCauseMessage = (null != rootCauseMessage) ? rootCauseMessage.toLowerCase() : DocumentConverterUtil.STR_NOT_AVAILABLE;
        String logMessage = null;

        if (StringUtils.isNotEmpty(rootCauseMessage)) {
            if (rootCauseMessage.length() > Properties.MAX_ERROR_MESSAGE_TOTAL_LENGTH) {
                final String fillStr = "...";
                final int beginLength = Properties.MAX_ERROR_MESSAGE_TOTAL_LENGTH - Properties.MAX_ERROR_MESSAGE_END_LENGTH - fillStr.length();
                final int endLength = Math.min(rootCauseMessage.length() - beginLength, Properties.MAX_ERROR_MESSAGE_END_LENGTH);

                logMessage = rootCauseMessage.substring(0, beginLength) + ((endLength > 0) ? ("..." + rootCauseMessage.substring(rootCauseMessage.length() - endLength)) : "");
            } else {
                logMessage = rootCauseMessage;
            }
        } else if (null != rootCause) {
            logMessage = rootCause.getClass().getName();
        }

        if (StringUtils.isEmpty(logMessage)) {
            logMessage = DocumentConverterUtil.STR_NOT_AVAILABLE;
        }

        if ((rootCause instanceof SocketException) ||
            (rootCause instanceof SocketTimeoutException) ||
            (rootCause instanceof EOFException) ||
            ((rootCause instanceof IOException) &&
             ((lowerCaseRootCauseMessage.contains("broken pipe") ||
               lowerCaseRootCauseMessage.contains(("connection reset")) ||
               lowerCaseRootCauseMessage.contains(("invalid stream")))))) {

            // ignore but trace socket exceptions; these may occur, when a connection timeout has happened
            LOG.trace("DC caught an ignorable connection exception: {}", logMessage);
        } else if (e instanceof OXException) {
            // #DOCS-3981: recognize for specified log level for log category in case of OXException
            final OXException oxExcp = (OXException) e;
            final Category category = oxExcp.getCategory();
            final LogLevel logLevel = (null != category) ?
                (((Category.CATEGORY_USER_INPUT == category) || (Category.CATEGORY_PERMISSION_DENIED == category)) ? LogLevel.DEBUG : category.getLogLevel()) :
                    LogLevel.ERROR;

            switch (logLevel) {
                case WARNING: {
                    LOG.warn("DC caught a WARNING exception: {}", logMessage);
                    break;
                }

                case INFO: {
                    LOG.info("DC caught an INFO exception: {}", logMessage);
                    break;
                }

                case DEBUG: {
                    LOG.debug("DC caught a DEBUG exception: {}", logMessage);
                    break;
                }

                case TRACE: {
                    LOG.trace("DC caught a TRACE exception: {}", logMessage);
                    break;
                }

                case ERROR:
                default: {
                    LOG.error("DC caught an exception: {}", logMessage);
                    break;
                }
            }
        } else {
            LOG.error("DC caught an exception: {}", logMessage);
        }
    }

    /**
     * @param logMessage
     * @param e
     */
    static public void logExcp(final String logMessage, final Exception e) {
        if (null != LOG) {
            LOG.error(logMessage, e);
        }
    }

    /**
     * @param logType
     * @param message
     * @param jobProperties
     * @param extraData
     */
    public static String implGetLogMessage(final String message, HashMap<String, Object> jobProperties, LogData... extraData) {
        final StringBuilder logMessageBuilder = (new StringBuilder(message.length() << 1)).append(message);
        StringBuilder infoStrBuilder = null;
        String fileName = null;

        if (null != jobProperties) {
            fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        }

        final boolean addClosingBrace = (extraData.length > 0) || (null != fileName);

        if (extraData.length > 0) {
            logMessageBuilder.append(" (");

            for (int i = 0; i < extraData.length; ++i) {
                final LogData logData = extraData[i];

                if (null != logData) {
                    if (i > 0) {
                        logMessageBuilder.append(", ");
                    }

                    logMessageBuilder.append(logData.getKey());
                    logMessageBuilder.append('=');

                    final String value = logData.getValue();
                    logMessageBuilder.append((null != value) ? value : "null");
                }
            }
        }

        // add decoded file name
        if (null != fileName) {
            logMessageBuilder.
                append((extraData.length > 0) ? ", " : " (").append("filename=").
                append(DocumentConverterManager.getFilenameForLog(fileName));
        }

        if ((null != jobProperties) && !jobProperties.isEmpty()) {
            for (final String key : jobProperties.keySet()) {
                // log all info_ properties but info_Filename only in case of an error
                if (key.startsWith(Properties.INFO_PREFIX) && !key.equals(Properties.PROP_INFO_FILENAME)) {
                    final Object obj = jobProperties.get(key);

                    if (null != infoStrBuilder) {
                        infoStrBuilder.append(", ");
                    } else {
                        infoStrBuilder = new StringBuilder(" (jobproperties: ");
                    }

                    infoStrBuilder.append(key.substring(Properties.INFO_PREFIX.length())).append('=').append(
                        (null != obj) ? obj.toString() : "null");
                }
            }
        }

        if (null != infoStrBuilder) {
            logMessageBuilder.append(infoStrBuilder);
        }

        if (addClosingBrace) {
            logMessageBuilder.append(')');
        }

        return logMessageBuilder.toString();
    }

    // - Static members --------------------------------------------------------

    private static Logger LOG = LoggerFactory.getLogger(DocumentConverterManager.class);

    // - Members ---------------------------------------------------------------

    protected File m_tmpRootDir;
}
