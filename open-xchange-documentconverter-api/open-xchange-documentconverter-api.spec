Name:          open-xchange-documentconverter-api
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
BuildRequires: open-xchange-core >= @OXVERSION@
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       The Open-Xchange backend for the documentconverter API
Requires:      open-xchange-core >= @OXVERSION@

%description
This package contains the backend components for the documentconverter API

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*

%changelog
* Thu Nov 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.3 release
* Thu Nov 21 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.3 release
* Thu Oct 17 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.3 release
* Tue Jun 18 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.3 release
* Thu May 02 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.1 release
* Mon Sep 10 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.1
* Mon Jun 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview of 7.10.0 release
* Tue Feb 20 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview of 7.10.0 release
* Fri Feb 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.0 release
* Fri Dec 01 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.0 release
* Tue May 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.8.4 release
* Mon Apr 03 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second release candidate for 7.8.3 release
* Thu Nov 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First release candidate for 7.8.3 release
* Tue Nov 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview for 7.8.3 release
* Sat Oct 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.8.3 release
* Fri Oct 14 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.8.2 release
* Tue Jun 14 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Thu Apr 07 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.2 release
* Wed Mar 30 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.1 release
* Thu Mar 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.1 release
* Tue Mar 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.8.1 release
* Fri Mar 04 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview of 7.8.1 release
* Fri Feb 19 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.1 release
* Tue Feb 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.1 release
* Tue Jan 26 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.1 release
* Fri Oct 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.1 release
* Fri Oct 02 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.8.0 release
* Fri Sep 25 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fith candidate for 7.8.0 release
* Fri Sep 18 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.8.0 release
* Mon Sep 07 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.0 release
* Fri Aug 21 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.0 release
* Wed Aug 05 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.0 release
* Tue Mar 10 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.0 release
* Mon Mar 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Twelfth candidate for 7.6.2 release
* Fri Mar 06 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eleventh candidate for 7.6.2 release
* Wed Mar 04 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Tenth candidate for 7.6.2 release
* Tue Mar 03 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Nineth candidate for 7.6.2 release
* Tue Feb 24 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.6.2 release
* Wed Feb 11 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.2 release
* Fri Jan 30 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.2 release
* Tue Jan 27 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.2 release
* Fri Dec 12 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.2 release
* Fri Dec 05 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.2 release
* Fri Nov 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.2 release
* Fri Oct 31 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.2 release
* Mon Oct 13 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.2 release
* Fri Oct 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.1 release
* Thu Oct 02 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.1 release
* Tue Sep 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.1 release
* Mon Sep 08 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.1 release
* Wed Aug 20 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Tue Aug 19 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Wed Jul 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-07-30
* Mon Jul 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.1
* Mon Jun 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.0 release
* Fri Jun 20 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.0 release
* Fri Jun 13 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.0 release
* Fri May 30 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.0 release
* Fri May 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.0 release
* Mon May 05 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.0 release
* Fri May 02 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-05-05
* Fri Apr 11 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.0 release
* Wed Mar 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 Spreadsheet Final release
* Wed Feb 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.0 spreadsheet release
* Wed Feb 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 spreadsheet release
* Fri Feb 07 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.4.2 release
* Thu Feb 06 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.2 release
* Tue Feb 04 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.2 release
* Thu Jan 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.4.2 release
* Mon Dec 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.4.2 release
* Mon Dec 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 release
* Thu Dec 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.2
* Wed Dec 11 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
sprint 21
* Wed Nov 20 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.1 release
* Fri Nov 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.1 release
* Thu Nov 07 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.4.1 release
* Wed Oct 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.4.1 release
* Thu Oct 10 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First sprint increment for 7.4.1 release
* Tue Sep 24 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Eleventh candidate for 7.4.0 release
* Fri Sep 20 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.1 release
* Fri Sep 20 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Tenth candidate for 7.4.0 release
* Thu Sep 12 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Ninth candidate for 7.4.0 release
* Sun Sep 01 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.4.0 release
* Tue Aug 27 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.4.0 release
* Fri Aug 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.4.0 release
* Mon Aug 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.0 release
* Tue Aug 13 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.0 release
* Tue Aug 06 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third release candidate for 7.4.0
* Fri Aug 02 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second release candidate for 7.4.0
* Wed Jul 17 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First release candidate for 7.4.0
* Tue Jul 16 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.0
* Mon Jul 01 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.2.2 release
* Fri Jun 28 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.2.2 release
* Wed Jun 26 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Release candidate for 7.2.2 release
* Fri Jun 21 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second feature freeze for 7.2.2 release
* Mon Jun 17 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Feature freeze for 7.2.2 release
* Mon Jun 03 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First sprint increment for 7.2.2 release
* Wed May 29 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.2.2 release
* Mon May 27 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.2.2
* Fri May 17 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.2.1 release
* Wed May 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.2.1 release
* Mon Apr 22 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.2.1 release
* Mon Apr 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.0
* Mon Apr 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.2.1
* Wed Apr 10 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.2.0 release
* Mon Apr 08 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.2.0 release
* Tue Apr 02 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.2.0 release
* Tue Mar 26 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First release candidate for 7.2.0
* Fri Mar 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.2.0
* Tue Feb 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth release candidate for 7.0.1
* Tue Feb 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third release candidate for 7.0.1
* Thu Feb 14 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second release candidate for 7.0.1
* Fri Feb 01 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First release candidate for 7.0.1
* Wed Dec 19 2012 Kai Ahrens <kai.ahrens@open-xchange.com>
first build
